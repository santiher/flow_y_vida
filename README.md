# Flow: el camino de la vida

## Running the game

Install the package and run `flow_server`.  
Hitting the api examples:
```
wget -q -O - http://127.0.0.1:8888/store
wget -q -O - http://127.0.0.1:8888/venues
wget -q -O - http://127.0.0.1:8888/rapper/ray@flow.com
```

Creating a rapper:
```
curl -d '{"name":"Ray Jones", "email": "ray@flow.com", "style": "Melodic", "password": "hola"}' \
     -H "Content-Type: application/json" \
     -X POST \
     http://localhost:8888/rapper
```

Relax:
```
curl -d '{"email": "ray@flow.com", "action": "relax"}' \
     -H "Content-Type: application/json" \
     -X POST \
     http://localhost:8888/action
```

Login:
```
curl -d '{"password": "hola"}' \
     -H "Content-Type: application/json" \
     -X POST \
     http://localhost:8888/login/ray2@flow.com
```

Or with docker and docker-compose:
```
cd docker/sever
./docker.sh run
```

By default the server is up on http://localhost:8888

On http://localhost:8888 there is a simple web interface to play the game.

## Structure

```
|\_ backend: python module with general and example code to build a web server.
|\_ data: csv with the items and venues info.
|\_ docker: contains docker files for running everything on docker.
|\_ flow: python module with the main functionalities of the game.
    |\_ server: server code.
        |\_ web: unfinished web interface for the game.
|\_ scripts: executable scripts to run the game.
|\_ storage: python module to store the game data somehow.
|\_ tests
    |\_ test\_<python_module>: unit tests for the module.
```

## To do

### Game

#### Done

* [x] Credit card
* [x] Credit card tests
* [x] Concert
* [x] Concert tests
* [x] Investments
* [x] Investments: discographic record
* [x] Investments: fashion brand
* [x] Investments: video productor
* [x] Loan
* [x] Manager advices
* [x] Manager translating documents to the rapper
* [x] Plazo fijo
* [x] Rankings: top 10
* [x] Rapper: as_dict
* [x] Rapper: perform_for
* [x] Rapper: relax
* [x] Store to buy stuff
* [x] Unexpected events: chance of suffering an unexpected event
* [x] World: buy_item
* [x] World: concert
* [x] World: make_investment
* [x] World: pay_credit_card
* [x] World: pay_loan
* [x] World: relax
* [x] World: request_loan
* [x] World: request_new_credit_card_limit
* [x] World: request_plazo_fijo
* [x] World: pass_week

### Tests

* [ ] Investments tests
* [ ] Loan tests
* [ ] Manager tests
* [ ] Plazo fijo tests
* [ ] Rapper tests
* [ ] Store tests
* [ ] World tests

### To improve

* [ ] Rapper: generalize investments / loans / plazos fijos / tarjeta?
* [ ] Generalize statements?
* [ ] World tests: cambiar objetos por Mocks y solo ver que se llamen las funciones?

### Api

* [x] Interact with the game though an API.
* [x] Log all hits

### Storage

* [x] Use a shelve to save the rappers

## API

Endpoints:

* /action
* /investments
* /login
* /ranking
* /rapper
* /store
* /tips
* /venues
* /word

### Action

#### Post
You post an action to be performed by a rapper along with its arguments:

* buy_item
* concert
* make_investment
* pay_credit_card
* pay_loan
* relax
* request_loan
* request_new_credit_card_limit
* request_plazo_fijo

Gets the rapper back, along with advice, updates about the world, and the
result  of the action.

Template:
Eg:
```
{'advice': [string],
 'rapper': Rapper,
 'result': string,
 'updates': [string]}
```

Eg:
```
{'advice': ['Te ves cansado Ray, deberias relajarte un rato.'],
 'rapper': {'certificates_of_deposit': [],
            'credit_card': {'available_limit': 2000,
                            'balance': 0,
                            'current_expenses': 0,
                            'interest_rate': 0.14,
                            'last_closure_week': 0,
                            'late_fees_charge': 100,
                            'limit': 2000,
                            'minimum_pay_rate': 0.1,
                            'next_closure_balance': 0.0,
                            'next_closure_minimum_pay': 0.0,
                            'next_closure_week': 4},
            'email': 'ray@flow.com',
            'fans': 300,
            'flow': 50,
            'investments': [{'type': 'DiscographicRecord'}],
            'loans': [],
            'name': 'Ray Jones',
            'style': 0,
            'wallet': {'cash': 34820},
            'week': 1},
 'result': 'Ray Jones has dado un gran concierto en BitFlow, han asistido 300 '
           'personas y tienes 29 nuevos fans!!\n'
           'Con unos ingresos de $30000, has obtenido una ganancia neta de '
           '$29820.',
 'updates': ['Ha terminado tu deposito a plazo fijo, ahora cuentas con $200.']}
```

### Investments

#### Get

Get the available investment posibilities and their characteristics.

```
[{"name": "Sello discografico",
  "money_cost": 10000,
  "flow_cost": 100}]
```

### Login

#### Post

If when creating a rapper you passed the argument *password*, you will be able
to login the rapper at `/login/ray@flow.com` passing `{"password": "hola"}`.

Eg login successful:
```
{"ok": 1}
```

Eg login unsuccessful:
```
{"ok": 0}
```

### Ranking

#### Get

Get the top 10 rankings.
A rapper email can be provided, if an email is given, the position of the
rapper in the ranking will be provided too.

Eg: /ranking
```
{
  "fans": [{"name": "Ray Jones", "value": 17},
           {"name": "Sugar Magic", "value": 12}],
  "flow": [{"name": "Sugar Magic", "value": 1023},
           {"name": "Ray Jones", "value": 831}],
  "money": [{"name": "Ray Jones", "value": 103},
           {"name": "Sugar Magic", "value": 84}]
}
```

Eg: /ranking/ray@flow.com
```
{
  "fans": [{"name": "Ray Jones", "value": 17},
           {"name": "Sugar Magic", "value": 12}],
  "flow": [{"name": "Sugar Magic", "value": 1023},
           {"name": "Ray Jones", "value": 831}],
  "money": [{"name": "Ray Jones", "value": 103},
           {"name": "Sugar Magic", "value": 84}]
  "rapper": {
              "fans": 1,
              "flow": 2
              "money": 1,
            },
  "rappers": 2
}
```

### Rapper

For get and delete include the email: rapper/ray@flow.com
The get and post follow the same format as the /action endpoint.

```
{'advice': [],
 'rapper': Rapper,
 'result': null,
 'updates': []}
```

#### Get

Gets the rapper.
```
{'cash': 50,
 'certificates_of_deposit': [],
 'credit_card': {'available_limit': 2000,
                 'balance': 0,
                 'current_expenses': 0,
                 'interest_rate': 0.14,
                 'last_closure_week': 0,
                 'late_fees_charge': 100,
                 'limit': 2000,
                 'minimum_pay_rate': 0.1,
                 'next_closure_balance': 0.0,
                 'next_closure_minimum_pay': 0.0,
                 'next_closure_week': 4},
 'email': 'ray@flow.com',
 'fans': 3200,
 'flow': 50,
 'investments': [{'type': 'DiscographicRecord'}],
 'loans': [],
 'name': 'Ray Jones',
 'style': 0,
 'wallet': {'cash': 0},
 'week': 0}
```

#### Delete

Deletes the rapper.

#### Post

Creates a new rapper. Needs an object with:

* email
* name
* style: Melodic, Lyric, Aggresive
* any extra variables you want to store, like images.

Returns the rapper.

### Store

#### Get

Returns a list of items as objects. Images and other variables can be added.
```
{"items": [{"name": "Gaseosa", "price": 20, "flow": 10},
           {"name": "Gorra", "price": 120, "flow": 40},
           {"name": "Lentes", "price": 500, "flow": 380}
           ]
 }
```

### Tips

#### Get

Returns a list of tips. If the rapper email is provided as tips/ray@flow.com,
then only the tips that the rapper has already seen will be returned

```
[{0: "Rajemos, nos viene a buscar la AFIP"}
 ]
```

### Venues

#### Get

Returns a list of venues as objects. Images and other variables can be added.
```
[{"name": "BitFlow", "cost": 18000, "capacity": 300},
 {"name": "Makema", "cost": 22000, "capacity": 320}
 ]
```

### Words

#### Get

Returns an amount of random words.

Endpoint: /words/"language_code"/"number"

Example: /words/es/10

Would return 10 spanish words.

## Action arguments

Examples:

```
{"action": "concert",
 "rapper": "ray_jones@flow.com",
 "venue_id": "luna_parque",
 "win": true}

{"action": "make_investment",
 "rapper": "ray_jones@flow.com",
 "investment_type": "DiscographicRecord"}

{"action": "buy_item",
 "rapper": "ray_jones@flow.com",
 "item_id": "gorra",
 "payment_method": "cash" / "credit"}

{"action": "pay_credit_card",
 "rapper": "ray_jones@flow.com",
 "amount": 24}

{"action": "pay_loan",
 "rapper": "ray_jones@flow.com",

{"action": "relax",
 "rapper": "ray_jones@flow.com"}

{"action": "request_new_credit_card_limit",
 "rapper": "ray_jones@flow.com",
 "new_limit": 10000}

{"action": "request_loan",
 "rapper": "ray_jones@flow.com",
 "amount": 10000,
 "interest_rate": 0.18,
 "duration": 10}

{"action": "request_plazo_fijo",
 "rapper": "ray_jones@flow.com",
 "amount": 100000,
 "interest_rate": 0.3,
 "duration": 4}
```
