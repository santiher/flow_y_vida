"""This module contains a rapper"""

import logging

from flow.constants import CashPayment, CreditPayment, MaxRelaxsInARow
from flow.credit_card import CreditCard
from flow.exceptions import (FlowException, LoanNotFoundException,
                             TooRelaxedException)
from flow.loan import Loan
from flow.utils import hash_class, repr_class
from flow.wallet import Wallet


logger = logging.getLogger(__name__)


class Rapper:

    def __init__(self, email, name, style, flow, fans, week=1, wallet=None,
                 credit_card=None, loans=None, investments=None,
                 certificates_of_deposit=None, seen_tips=None, **extra_info):
        """This is a rapper like Ray Jones. Not much to explain.

        Parameters
        ----------
        email: str
            The rapper's email
        name: str
            The rapper's name
        style: Style
            The rapper's style
        flow: int
            The amount of flow the rapper has available
        fans: int
            The amount of fans the rapper has
        week: int
            The weeks since the rapper career started
        wallet: Wallet
            The rapper's cash wallet
        credit_card: CreditCard
            The rapper's credit card
        loan: [Loan]
            The rapper's list of ongoing loans
        investments: [Investment]
            The rapper's list of investments
        certificates_of_deposit: [CertificateOfDeposit]
            The rapper's list of plazos fijos
        seen_tips: set(int)
            The ids of the manager tips that the rapper has already seen.
        extra_info: kwargs dict
            Any extra variables that wants to be stored, not related to the
            rules. This variables will be forwarded to the as_dict method.
            Eg: image url.
        """
        self.email = email
        self.name = name
        self.style = style
        self.flow = flow
        self.fans = fans
        self.week = week
        self.wallet = wallet if wallet is not None else Wallet(420)
        self.credit_card = credit_card if credit_card else CreditCard()
        self.loans = loans if loans else []
        self.investments = investments if investments else []
        self.certificates_of_deposit = (certificates_of_deposit if
                                        certificates_of_deposit else [])
        self.seen_tips = seen_tips if seen_tips else set()
        self.extra_info = extra_info
        self.relax_in_a_row = 0

    @property
    def net_worth(self):
        """Returns the rapper net worth."""
        actives = (self.wallet.cash +
                   self.credit_card.payed +
                   sum(i.money_cost for i in self.investments) +
                   sum(cd.money for cd in self.certificates_of_deposit) +
                   sum(l.payed for l in self.loans))
        pasives = (self.credit_card.balance +
                   self.credit_card.current_expenses +
                   sum(l.debt for l in self.loans))
        return actives - pasives

    def pay(self, amount):
        """The rapper is payed in cash, which is added to his wallet

        Parameters
        ----------
        amount: float
            The amount of cash to add to his wallet

        """
        self.wallet.add(amount)

    def charge(self, amount, method):
        """The rapper is charged a certain amount of money.

        Parameters
        ----------
        amount: float
            The amount of money the rapper has to pay
        method: str
            The payment method: cash/credit

        """
        if method == CashPayment:
            self.wallet.pay(amount)
        elif method == CreditPayment:
            self.credit_card.pay(amount)
        else:
            raise FlowException('Payment method does not exist')

    def perform_for(self, amount_of_people):
        """The rapper performs his art for a the given amount of people.
        The rapper loses / gains flow depending on the amount of people.

        Parameters
        ----------
        amount_of_people: int
            The amount of people the rapper performs a concert for.

        Returns
        -------
        int: amount of flow lost/gained
        """
        self.relax_in_a_row = 0
        if amount_of_people % 5 == 0:
            flow = 0.1 * amount_of_people
        else:
            flow = -0.2 * amount_of_people
        self.flow += flow
        if self.flow < 0:
            self.flow = 0
        return flow

    def acknowledge_new_fans(self, fans):
        """The rapper gains new fans.

        Parameters
        ----------
        fans: int
            Amount of fans that the rapper adds.
        """
        self.fans += fans

    def lose_fans(self, fans):
        """The rapper loses fans.

        Parameters
        ----------
        fans: int
            Amount of fans that the rapper loses.
        """
        self.fans -= min(fans, self.fans)

    def gain_flow(self, flow):
        """The rapper gains flow.

        Parameters
        ----------
        flow: int
            Amount of flow
        """
        self.flow += flow

    def lose_flow(self, flow):
        """The rapper loses flow.

        Parameters
        ----------
        flow: int
            Amount of flow
        """
        self.flow -= min(flow, self.flow)

    def buy(self, store, item_id, payment_method):
        """Buy an item from the store and get flow.

        Parameters
        ----------
        fans: int

        Returns
        -------
        item: item
        payment_method: payment_method
        """
        ticket = store.sell(self, item_id, payment_method)
        self.flow += ticket.flow
        return ticket

    def relax(self):
        """The rapper relaxes and recovers some flow.

        Returns
        -------
        Relax
        """
        if self.relax_in_a_row > MaxRelaxsInARow:
            raise TooRelaxedException
        self.relax_in_a_row += 1
        try:
            flow_gain = int(0.5 * (self.flow ** 0.5 + self.fans ** 0.5))
        except TypeError:
            logging.error('Error in relax', exc_info=True)
            flow_gain = 14
        self.flow += flow_gain
        return Relax(self.name, flow_gain)

    def add_investment(self, investment):
        """The rapper gets his new investment."""
        self.investments.append(investment)

    def add_loan(self, loan):
        """The rapper gets his new loan."""
        self.loans.append(loan)

    def add_certificate_of_deposit(self, cd):
        """The rapper gets his new cd."""
        self.certificates_of_deposit.append(cd)

    def pay_credit_card(self, amount):
        """Tries to pay the credit card or part of it."""
        return self.credit_card.cancel(self, amount)

    def pay_loan(self, loan_id, amount):
        """Tries to pay a loan or part of it."""
        for loan in self.loans:
            if loan.id == loan_id:
                return loan.cancel(self, amount)
        raise LoanNotFoundException

    def check_credit_card(self):
        """Returns the credit card statement if its period closed."""
        if self.credit_card.is_new_period(self.week):
            statement = self.credit_card.close_period(self.week)
            if (statement.payed > 0 or statement.balance > 0 or
                    statement.new_balance > 0):
                return statement

    def check_loans(self):
        """Returns the loans statements if their period closed."""
        statements = []
        actives = []
        for loan in self.loans:
            if loan.finished(self.week):
                statement = loan.close(self.name)
                statements.append(statement)
            else:
                actives.append(loan)
        self.loans = actives
        return statements

    def check_investments(self):
        """Returns the investments statements payout if their period closed."""
        statements = []
        for investment in self.investments:
            statement = investment.check(self)
            if statement:
                statements.append(statement)
        return statements

    def check_certificates_of_deposit(self):
        """Returns the plazo fijo statements if they finished."""
        actives = []
        statements = []
        for cd in self.certificates_of_deposit:
            statement = cd.check(self)
            if statement:
                statements.append(statement)
            else:
                actives.append(cd)
        self.certificates_of_deposit = actives
        return statements

    def _data(self):
        return (self.name, self.style)

    def as_dict(self):
        return {**self.extra_info,
                'email': self.email,
                'name': self.name,
                'style': self.style,
                'flow': self.flow,
                'flow_percentage': self.flow / 1000,
                'fans': self.fans,
                'week': self.week,
                'wallet': self.wallet.as_dict(),
                'credit_card': self.credit_card.as_dict(),
                'loans': [loan.as_dict() for loan in self.loans],
                'investments': [investment.as_dict() for investment in
                                self.investments],
                'certificates_of_deposit': [cd.as_dict() for cd in
                                            self.certificates_of_deposit],
                'password': None,
                'credit_card_max_limit': self.credit_card.max_limit(self),
                'loan_max_amount': Loan.max_amount(self),
                }


class Relax:

    def __init__(self, rapper_name, flow):
        self.rapper_name = rapper_name
        self.flow = flow

    def _data(self):
        return self.rapper_name, self.flow

    __hash__ = hash_class
    __repr__ = repr_class
