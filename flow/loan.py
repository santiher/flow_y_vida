"""This module contains the code to handle loans, simple loans, one time pay,
No french or german loans bullshit
"""

from random import Random

from flow.constants import CashPayment, YearWeeks
from flow.exceptions import LoanNotGrantedException
from flow.utils import eq_class, repr_class


LoanRandom = Random(14)


class Loan:

    def __init__(self, amount, interest_rate, duration, start_week, id=None):
        """A cash loan.

        Parameters
        ----------
        amount: float
            The amount of money requested.
        interest_rate: float
            The interest rate.
        duration: int
            The duration in weeks.
        start_week: int
            The start week
        id: int / None
            The loan id
        """
        self.id = id or LoanRandom.randint(0, 1073741824)
        self.amount = amount
        self.interest_rate = interest_rate
        self.duration = duration
        self.start_week = start_week
        self.debt = amount * (1 + interest_rate) ** (duration / YearWeeks)
        self.payed = 0

    def status(self):
        """Returns the loan's status.

        Intended for more complicated loans like the french / german
        """
        raise NotImplementedError

    def finished(self, current_week):
        """Returns true if the loan finished.
        If the duration excedeed but it is not fully payed, extend it."""
        if self.payed >= self.debt:
            return True
        elif current_week >= self.start_week + self.duration:
            self.debt *= (1 + self.interest_rate) ** (1 / YearWeeks)

    def close_period(self, week):
        """Passes to next period.
        Intended for more complicated loans.
        """
        raise NotImplementedError

    def close(self, owner_name):
        """Finished the loan and returns it's statement"""
        return LoanCancelled(owner_name, self.amount, self.payed)

    def cancel(self, rapper, amount):
        """Tries to pay part of the loan with cash
        Parameters
        ----------
        rapper: Rapper
            The rapper owner of the loan
        amount: float
            The amount of money payed

        Returns
        -------
        LoanPaymentNote
        """
        amount = min(amount, self.debt - self.payed)
        rapper.charge(amount, CashPayment)
        self.payed += amount
        return LoanPaymentNote(rapper.name, amount, self.debt - self.payed)

    @staticmethod
    def request_loan(rapper, amount, interest_rate, duration):
        """Request a new loan. This should be the way to generate loans.
        If everything is ok, the loan will be granted, created and added to the
        rapper.
        If things are not ok, the loan won't be granted.

        Parameters
        ----------
        rapper: Rapper
            The rapper requesting the loan.
        amount: float
            The amount of money requested.
        interest_rate: float
            The loan's interest rate.
        duration: int
            The loan's duration in weeks.

        Returns
        -------
        Loan
        """
        strikes = 0
        if amount > Loan.max_amount(rapper):
            raise LoanNotGrantedException
        for loan in rapper.loans:
            if (rapper.week >= loan.start_week + loan.duration and
                    loan.payed < loan.debt):
                strikes += 1
        if strikes >= 3:
            raise LoanNotGrantedException
        loan = Loan(amount, interest_rate, duration, rapper.week)
        rapper.add_loan(loan)
        rapper.wallet.add(amount)
        return loan

    def max_amount(rapper):
        """Returns the maximum amount that will be approved to a rapper.

        Parameters
        ----------
        rapper: Rapper
            The rapper the amount has to be evaluated for.

        Returns
        -------
        int:
            the maximum limit
        """
        return 100 * rapper.fans

    def as_dict(self):
        return {'amount': self.amount, 'interest_rate': self.interest_rate,
                'duration': self.duration, 'start_week': self.start_week,
                'debt': self.debt, 'payed': self.payed, 'id': self.id,
                'remaining_debt': self.debt - self.payed}

    def _data(self):
        return (self.amount, self.interest_rate, self.duration,
                self.start_week, self.id)

    __repr__ = repr_class
    __eq__ = eq_class


class LoanPaymentNote:

    def __init__(self, owner_name, amount, remaining_debt):
        self.owner_name = owner_name
        self.amount = amount
        self.remaining_debt = remaining_debt


class LoanCancelled:

    def __init__(self, owner_name, amount, payed):
        self.owner_name = owner_name
        self.amount = amount
        self.payed = payed
