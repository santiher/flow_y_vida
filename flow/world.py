"""This module contains the flow life world.

It represents and is in charge of what happens every week and after every
action.

Rapper actions:
- buy_item
- concert
- make_investment
- pay_credit_card
- pay_loan
- relax
- request_loan
- request_new_credit_card_limit
- request_plazo_fijo

Each action that a rapper can happen in the world and is performed by the
rapper has to be implemented as:

class World:

    def _{action_name}(self, rapper, **action_kwargs):
        '''Performs the action {action_name} by the rapper.

        Returns
        -------
        Object, dictionary
            Object: the object returned by the action.
        '''
        raise NotImplementedError
"""

from flow.certificate_of_deposit import CertificateOfDeposit
from flow.exceptions import FlowException
from flow.investment import investment_types
from flow.loan import Loan

WeekPassingActions = 'concert', 'relax'


class World:

    def __init__(self, venues, store):
        """The world is the object that sees how life moves on on Flow life.

        Parameters
        ----------
        venues: {venue_id: Venue}
            A dictionary of venues available in the game.
        store: Store
            The store available in the world which contains items for sale.
        """
        self.venues = venues
        self.store = store

    def act(self, rapper, action, *args, **kwargs):
        """Make a rapper perform an action and see what happens.
        Might also update the rapper if a week passes.

        Parameters
        ----------
        rapper: Rapper
            The rapper that will perform the action.
        action: str
            The action name that has to be performed. eg: perform.
        args: iterable
            The arguments to send to the action.
        kwargs: dict
            The keyword arguments to send to the action.

        Returns
        -------
        Object, dictionary
            The object that returns the action performed
            New week's updates
                Used for example for credit card statements, plazo fijos,
                investments, loans, etc.
                If the action does not pass a week, it be empty.

                It has the following format:
                {"keys": ["statement"], "statement": Statement}
                The "keys" of the objects to look at.
        """
        if not hasattr(self, f'_{action}'):
            raise FlowException('The action does not exist.')
        ret = getattr(self, f'_{action}')(rapper, *args, **kwargs)
        if action in WeekPassingActions:
            updates = self.pass_week(rapper)
        else:
            updates = None
        return ret, updates

    def pass_week(self, rapper):
        """Passes a week in the rapper life

        Parameters
        ----------
        rapper: Rapper
            The rapper whose life is moving on

        Returns
        -------
        updates: list
            A list of updates containing statements
        """
        updates = []
        updates.append(rapper.check_credit_card())
        updates.extend(rapper.check_loans())
        updates.extend(rapper.check_investments())
        updates.extend(rapper.check_certificates_of_deposit())
        updates = [x for x in updates if x is not None]
        rapper.week += 1
        return updates

    def _concert(self, rapper, venue_id, win):
        """Give a concert at the venue.

        Parameters
        ----------
        rapper: Rapper
            The rapper that will give the concert.
        venue_id: str/id
            The venue's id as used in self.venues
        win: boolean
            True: The rapper won the concert and gets a good amount of fans
            False: the rapper performed poorly on the battle and gets less fans

        Returns
        -------
        Concert
            The concert's result.
        """
        try:
            venue = self.venues[venue_id]
        except KeyError:
            raise FlowException(
                'The venue_id does not correspond to a valid venue.')
        concert = venue.concert(rapper, win)
        return concert

    def _buy_item(self, rapper, item_id, payment_method):
        """Buys an item at the store

        Parameters
        ----------
        rapper: Rapper
            The rapper that will buy the stuff.
        item_id: str/id
            The item's id as it is in the store.
        payment_method: str
            The payment method for the item
        """
        return rapper.buy(self.store, item_id, payment_method)

    def _make_investment(self, rapper, investment_type):
        """Make an investment.

        Parameters
        ----------
        rapper: Rapper
            The rapper that will give the concert.
        investment_type: str/id
            The name of the investment type.

        Returns
        -------
        Investment
        """
        try:
            Investment = investment_types[investment_type]
            investment = Investment.invest(rapper)
            return investment
        except KeyError:
            raise FlowException(
                'The investment type does not exist.')

    def _relax(self, rapper):
        """The rapper just relax for the rest of the week to gain some flow.
        """
        return rapper.relax()

    def _request_new_credit_card_limit(self, rapper, amount):
        """Request a new limit for the credit card

        Parameters
        ----------
        rapper: Rapper
            The rapper whose credit card limit will be updated.
        new_limit: float
            The new credit card value you want to request

        Returns
        -------
        Bool
            Whether the new limit was accepted or not
        """
        return rapper.credit_card.change_limit(amount, rapper)

    def _pay_credit_card(self, rapper, amount):
        """Pay the credit card.

        Parameters
        ----------
        rapper: Rapper
            The rapper who will be paying the credit card.
        amount: float
            The amount of money to pay, does not have be all the balance.

        Returns
        -------
        Credit card payment note
            The credit card's payment note.
        """
        amount = float(amount)
        return rapper.pay_credit_card(amount)

    def _pay_loan(self, rapper, amount, loan_id):
        """Pay the loan

        Parameters
        ----------
        rapper: Rapper
            The rapper who will be paying the credit card.
        amount: float
            The amount of money to pay, does not have be everything.
        loan_id: int
            The id of the loan

        Returns
        -------
        Statement?
            The new loan's status
        """
        amount = float(amount)
        return rapper.pay_loan(loan_id, amount)

    def _request_loan(self, rapper, amount, interest_rate, duration):
        """Request a new loan.

        Parameters
        ----------
        rapper: Rapper
            The rapper
        amount: float
            Loan's amount
        interest_rate: float
            Loan's interest rate
        duration: float
            Loan's duration

        Returns
        -------
        Loan
            The new loan or a rejection note
        """
        amount = float(amount)
        interest_rate = float(interest_rate)
        duration = int(duration)
        return Loan.request_loan(rapper, amount, interest_rate, duration)

    def _request_plazo_fijo(self, rapper, amount, interest_rate, duration):
        """Request a new plazo fijo.

        Parameters
        ----------
        rapper: Rapper
            The rapper
        amount: float
            CD's amount
        interest_rate: float
            CD's interest rate
        duration: float
            CD's duration

        Returns
        -------
        CD
            The new CD or a rejection note
        """
        amount = float(amount)
        interest_rate = float(interest_rate)
        duration = int(duration)
        return CertificateOfDeposit.request(rapper, amount, interest_rate,
                                            duration)
