"""This module contains the code to handle cash wallets"""


from flow.exceptions import FlowException, InsufficientMoneyException


class Wallet:

    def __init__(self, cash):
        """A cash wallet use to pay stuff.
        Parameters
        ----------
        cash: float
            The amount of cash in the wallet
        """
        self.cash = cash

    def add(self, amount):
        """Tries to pay something with the credit card
        Parameters
        ----------
        amount: float
            The amount of cash to add to the wallet

        Returns
        -------
        Wallet
            Self: the wallet with the extra money
        """
        if amount < 0:
            raise FlowException('Tried to add negative cash to the wallet')
        self.cash += amount
        return self

    def pay(self, amount):
        """Tries to pay something with the cash
        Parameters
        ----------
        amount: float
            The amount of money to pay

        Returns
        -------
        CreditCard:
            The credit card
        """
        if amount < 0:
            raise FlowException('Tried to payed negative money with cash')
        if self.cash < amount:
            raise InsufficientMoneyException()
        self.cash -= amount
        return self

    def as_dict(self):
        return {'cash': self.cash}

    def __repr__(self):
        return 'Wallet(%s)' % self.cash

    def __eq__(self, other_wallet):
        return self.cash == other_wallet.cash
