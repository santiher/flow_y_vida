"""This module contains the code to handle investments"""

import random

from flow.constants import CashPayment
from flow.exceptions import InsufficientFlowException
from flow.utils import hash_class, repr_class


class InvestmentStatement:

    def __init__(self, investment_type, payout, statement_week, rapper_name):
        """Investment statement showing the result of a period.

        Parameters
        ----------
        investment_type: str
            The name of the type of investment.
        payout: float
            Amount of money made / lost.
        statement_week: int
            Week number in the rapper's life.
        rapper_name: str
            The rapper owner of the investment's name
        """
        self.investment_type = investment_type
        self.payout = payout
        self.statement_week = statement_week
        self.rapper_name = rapper_name


class Investment:
    """A general investment that generates passive income for the rapper every
    week.

    Class
    -----
    money_cost: float
        The amount of cash it takes to make this investment
    flow_cost: float
        The amount of flow it takes to make this investment
    """

    money_cost = None
    flow_cost = None
    img = 'https://i.imgur.com/bMQKQOe.png'

    def __init__(self, start_week, last_period_week=None):
        """Returns a new investment.

        Parameters
        ----------
        start_week: int
            Number of the week the investment is being done
        last_period_week: int/None
            Number of the week the rapper last got results from the investment.
            Defaults to start_week
        """
        self.start_week = start_week
        self.last_period_week = last_period_week or start_week

    def get_result(self, rapper):
        """The rapper goes find out how much money he made / lost this period.
        This method should be implemented in the subclass

        Parameters
        ----------
        rapper: Rapper
            The owner of the investment

        Returns
        -------
        InvestmentStatement
            Contains the amount of money generated / lost
        """
        raise NotImplementedError

    def is_new_period(self, current_week):
        """Returns if the current week starts a new period

        Parameters
        ----------
        current_week: int
            The current week's number

        Returns
        ----------
        Bool
            True if a new period has been reached
        """
        return self.last_period_week < current_week

    def check(self, rapper):
        """Checks if a period has passed for the investment
        Finished object or None. And returns the money to the rapper.

        Parameters
        ----------
        rapper: Rapper

        Returns
        -------
        InvestmentStatement / None
            A statement if a period passed or None
        """
        if self.is_new_period(rapper.week):
            statement = self.get_result(rapper)
            if statement.payout >= 0:
                rapper.pay(statement.payout)
            else:
                remaining = max(0, rapper.wallet.cash + statement.payout)
                rapper.wallet.cash = remaining
            return statement

    @classmethod
    def invest(cls, rapper):
        """The way to create an investment, by investing in it.
        The rapper loses money, gains an investment.

        Parameters
        ----------
        rapper: Rapper
            The rapper making the investment

        Returns
        -------
        Investment
        """
        if cls.flow_cost > rapper.flow:
            raise InsufficientFlowException
        rapper.charge(cls.money_cost, CashPayment)
        investment = cls(rapper.week)
        rapper.add_investment(investment)
        rapper.flow -= cls.flow_cost
        return investment

    def as_dict(self):
        return {'type': self.__class__.__name__, 'start_week': self.start_week,
                'last_period_week': self.last_period_week}

    @classmethod
    def as_dict_cls(cls):
        return {'name': cls.__name__, 'money_cost': cls.money_cost,
                'flow_cost': cls.flow_cost, 'img': cls.img}

    def _data(self):
        return self.money_cost, self.flow_cost, self.start_week

    __hash__ = hash_class
    __repr__ = repr_class


class DiscographicRecord(Investment):

    money_cost = 10000
    flow_cost = 100
    img = 'https://i.imgur.com/bMQKQOe.png'

    def get_result(self, rapper):
        sign = 1 if rapper.fans > 500 else -1
        payout = 0.1 * sign * (rapper.wallet.cash ** 0.2 + rapper.fans)
        return InvestmentStatement(self.__class__.__name__, payout,
                                   rapper.week, rapper.name)


class FashionBrand(Investment):

    money_cost = 7000
    flow_cost = 400
    img = 'https://i.imgur.com/NDf3ij9.png'

    def get_result(self, rapper):
        sign = 1 if rapper.fans > 200 else -1
        payout = 7 * sign * rapper.fans ** 0.7
        return InvestmentStatement(self.__class__.__name__, payout,
                                   rapper.week, rapper.name)


class VideoProductor(Investment):

    money_cost = 30000
    flow_cost = 1000
    img = 'https://i.imgur.com/EsLY32B.png'

    def get_result(self, rapper):
        sign = 1 if rapper.fans > 1000 else -1
        payout = 60 * sign * rapper.fans ** 0.5
        return InvestmentStatement(self.__class__.__name__, payout,
                                   rapper.week, rapper.name)


class GraffitiTour(Investment):

    money_cost = 3000
    flow_cost = 80
    img = 'https://i.imgur.com/N5fTwgQ.png'

    def get_result(self, rapper):
        if rapper.fans < 500:
            payout = random.randint(70, 280)
        else:
            payout = int(random.randint(70, 280) * (1 + rapper.fans ** 0.1))
        return InvestmentStatement(self.__class__.__name__, payout,
                                   rapper.week, rapper.name)


class OnlineVideoChannel(Investment):

    money_cost = 4000
    flow_cost = 1000
    img = 'https://i.imgur.com/2J1sKem.png'

    def get_result(self, rapper):
        payout = int(0.1 * rapper.fans ** 0.5)
        return InvestmentStatement(self.__class__.__name__, payout,
                                   rapper.week, rapper.name)


class RapMuseum(Investment):

    money_cost = 50000
    flow_cost = 5000
    img = 'https://i.imgur.com/hsm1IDR.png'

    def get_result(self, rapper):
        payout = int(100 * 0.1 * rapper.fans ** 0.5)
        return InvestmentStatement(self.__class__.__name__, payout,
                                   rapper.week, rapper.name)


investment_types = {'DiscographicRecord': DiscographicRecord,
                    'FashionBrand': FashionBrand,
                    'VideoProductor': VideoProductor,
                    'GraffitiTour': GraffitiTour,
                    'OnlineVideoChannel': OnlineVideoChannel,
                    'RapMuseum': RapMuseum}
