import random


def uevent_0(rapper):
    """An unexpected event happens to the rapper and is modified

    Parameters
    ----------
    rapper: Rapper
        The rapper that may experience an unexpected event.
        The rapper may be modified as a consequence of the event.

    Return
    ------
    Tuple
    Returns the values necessary to format the notification string
    The notification string has to be defined in the language used in /lang
    """
    found_money = random.randint(70, 280)
    rapper.wallet.add(found_money)
    return found_money,


def uevent_1(rapper):
    penalty = min(random.randint(400, 1200), int(rapper.wallet.cash))
    rapper.wallet.cash -= penalty
    return rapper.name, penalty


def uevent_2(rapper):
    spent = random.randint(400, 700)
    flow = random.randint(20, 400)
    if spent > rapper.wallet.cash:
        spent = int(rapper.wallet.cash)
    rapper.wallet.cash -= spent
    rapper.gain_flow(flow)
    return flow, spent


def uevent_3(rapper):
    flow = random.randint(20, 80)
    rapper.gain_flow(flow)
    return flow,


def uevent_4(rapper):
    spent = random.randint(100, 200)
    if spent > rapper.wallet.cash:
        spent = int(rapper.wallet.cash)
    rapper.wallet.cash -= spent
    return rapper.name, spent


def uevent_5(rapper):
    flow = random.randint(30, 200)
    if flow > rapper.flow:
        flow = int(0.1 * rapper.flow)
    rapper.lose_flow(flow)
    return flow, rapper.name


def uevent_6(rapper):
    fans = random.randint(100, 500)
    rapper.acknowledge_new_fans(fans)
    return fans,


def uevent_7(rapper):
    if rapper.fans < 10:
        fans = min(3, rapper.fans)
    else:
        fans = random.randint(9, 9 + int(0.1 * rapper.fans))
    rapper.lose_fans(fans)
    return fans,


def uevent_8(rapper):
    if rapper.fans < 10:
        fans = min(3, int(rapper.fans))
    else:
        fans = random.randint(3, int(min(60, rapper.fans)))
    rapper.lose_fans(fans)
    return rapper.name, fans


def uevent_9(rapper):
    money = int(rapper.wallet.cash)
    if money < 100:
        donation = random.randint(0, money)
    else:
        donation = random.randint(100, min(money, 7000))
    fans = int(donation * 0.33)
    rapper.acknowledge_new_fans(fans)
    rapper.wallet.pay(donation)
    return donation, fans


def uevent_10(rapper):
    flow = min(random.randint(10, 350), rapper.flow)
    rapper.lose_flow(flow)
    return flow,


def uevent_11(rapper):
    flow = int(max(0.1 * rapper.fans ** 0.5, random.randint(19, 143)))
    rapper.gain_flow(flow)
    return flow,


def uevent_12(rapper):
    money = random.randint(300, max(2400, int(0.2 * rapper.fans)))
    rapper.pay(money)
    return money,


def uevent_13(rapper):
    money = random.randint(150, max(2400, int(0.5 * rapper.fans ** 0.5)))
    fans = int(money / random.randint(2, 40))
    rapper.pay(money)
    rapper.acknowledge_new_fans(fans)
    return money, fans


# End events


def is_event(varname):
    return varname.startswith('uevent_')


def number(varname):
    return int(varname.split('_')[1])


unexpected_events = [(number(varname), value)
                     for varname, value in globals().items()
                     if is_event(varname)]
unexpected_events = sorted(unexpected_events, key=lambda x: x[0])
