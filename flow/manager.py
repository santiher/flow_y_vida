from ast import literal_eval
from random import choice, random

from jinja2 import Template

from backend.utils import get_package_path
from flow.tips import tips
from flow.unexpected_events import unexpected_events


class Manager:
    """Symbolic class representing a manager"""

    def __init__(self, language='es'):
        path = '%s/flow/lang/%s' % (get_package_path(), language)
        with open(path, 'r') as f:
            self.lang = literal_eval(f.read())

    def work(self, rapper, action, action_result, updates):
        """
        Parameters
        ----------
        rapper: Rapper
            The rapper he is managing.
        action: str
            The action that performed the rapper.
        action_result: Document
            The document resulting from the rapper's last action.
        updates: [Document]
            The list of documents resulting from this week finishing.

        Returns
        -------
        Dictionary
            advice: str
                Advice that could help the rapper to progress in the game
            result: str
                The result of the rapper's action
            updates: [str]
                What happened this week in words.
            rapper: dict
                The updated rapper.
        """
        ret = {}
        ret['advice'] = self.advice(rapper, action, action_result)
        ret['result'] = self.translate(action_result)
        if updates is not None:
            ret['updates'] = [self.translate(update) for update in updates]
        else:
            ret['updates'] = []
        unexpected_event = self.unexpected_event(rapper)
        if unexpected_event:
            ret['updates'].append(unexpected_event)
        ret['rapper'] = rapper.as_dict()
        return ret

    @staticmethod
    def empty_msg():
        """Returns an empty 'work' message."""
        return {'rapper': None, 'advice': [], 'result': None, 'updates': []}

    def advice(self, rapper, action, action_result):
        """Returns advice messages based on the rapper's status.
        For example recommend to take a loan or to relax if the rapper has
        low money / low flow

        Parameters
        ----------
        rapper: Rapper
        action: str
        action_result: Document

        Returns
        -------
        [str]
        """
        advices = []
        for i, tip in tips:
            if tip(rapper, action, action_result):
                advices.append(self.lang['tips'][i])
                rapper.seen_tips.add(i)
        return advices

    def translate(self, document):
        """Translates documents and results into easy to understand advice for
        the rapper.
        It is single dispatched depending on the document type.
        Actual implementations are below, one function depending on the type of
        document being received.
        It is done with singledispatch to avoid lots of
        `if isinstance(document, Statement):` do that and so.

        Parameters
        ----------
        document: Object
            Some kind of document that results from one of the rappers actions.
            Like a Concert, PurchaseTicket, Statement, etc.

        Returns
        -------
        string explaining what happend
        """
        template = self.lang['messages'][document.__class__.__name__]
        return Template(template).render(obj=document, lang=self.lang)

    def explain_exception(self, exception):
        """
        Parameters
        ----------
        exception: FlowException
            The exception to be explained

        Returns
        -------
        str
            A textual explanation of the exception
        """
        exception = exception.__class__.__name__
        return dict(error=self.lang['exceptions'][exception])

    def translate_investments(self, investments):
        """
        Parameters
        ----------
        investments: [dict]
            A list of investments class dicts you want the manager to translate

        Returns
        -------
        [dict]
            A list of dict translating the values
        """
        return [{**i, 'nice_name': self.lang['names'][i['name']].capitalize()}
                for i in investments]

    def tell_me_tips(self, rapper=None):
        """
        Parameters
        ----------
        rapper: Rapper or None
            Rapper: returns all the tips the rapper has seen
            None: returns all tips

        Returns
        -------
        [str]
            A list of tips
        """
        if rapper is not None:
            return {i: self.lang['tips'][i] for i in rapper.seen_tips}
        return self.lang['tips']

    def unexpected_event(self, rapper):
        """
        Parameters
        ----------
        rapper: Rapper
            The rapper that may experience an unexpected event.
            The rapper may be modified as a consequence of the event.

        Returns
        -------
        str / None
            None if nothing happened
            A string explaining the event otherwise
        """
        if random() > 0.18:
            return
        i, event = choice(unexpected_events)
        ret = event(rapper)
        return self.lang['unexpected_events'][i].format(*ret)

    def welcome(self, rapper):
        """
        Parameters
        ----------
        rapper: Rapper
            The rapper the manager is welcomming

        Returns
        -------
        [str]
            A list of welcome messages
        """
        return [s.format(**rapper) for s in self.lang['welcome']]
