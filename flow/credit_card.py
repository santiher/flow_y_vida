"""This module contains the code to handle credit cards"""

from flow.constants import CashPayment
from flow.exceptions import FlowException, InsufficientLimitException
from flow.utils import hash_class, repr_class, eq_class


WeeksInPeriod = 4


class CreditCard:

    def __init__(self, interest_rate=0.80, minimum_pay_rate=0.10, limit=2000,
                 balance=0, current_expenses=0, late_fees=100,
                 last_period_week=0, payed=0):
        """A credit card.
        Parameters
        ----------
        interest_rate: float
            The monthly interest rate
        minimum_pay_rate: float
            The minimum rate the owner has to pay of the balance each month
        limit: float
            The balance's maximum amount
        balance: float
            The amount of money the owner owes to the credit card company
        current_expenses: float
            This month current expenses, at the end of the month the user can
            pay this amount without paying interests.
        late_fees: float
            Amount of extra money owed if you don't pay the minimum pay.
        last_period_week: int
            The week when the last period was closed.
        payed: int
            The amount of money payed on this period
        """
        self.interest_rate = interest_rate
        self.minimum_pay_rate = minimum_pay_rate
        self.limit = limit
        self.balance = balance
        self.current_expenses = current_expenses
        self.late_fees = late_fees
        self.last_period_week = last_period_week
        self.payed = payed

    def full_balance(self, current_week):
        """Total amount of money owed to the credit card company.
        Parameters
        ----------
        current_week: int
            The current week's number
        """
        current_expenses = self.current_expenses - self.payed
        if current_expenses < 0:
            balance = self.balance + current_expenses
            current_expenses = 0
        else:
            balance = self.balance
        if self.is_new_period(current_week):
            balance *= 1 + self.period_rate
        return balance + current_expenses

    def cancel(self, rapper, amount):
        """Tries to pay part of the loan with cash
        Parameters
        ----------
        rapper: Rapper
            The rapper owner of the loan
        amount: float
            The amount of money payed

        Returns
        -------
        self
        """
        to_pay = self.full_balance(self.last_period_week + WeeksInPeriod)
        max_to_pay = to_pay - self.payed
        amount = min(amount, max_to_pay)
        rapper.charge(amount, CashPayment)
        self.payed += amount
        return CreditCardPaymentNote(rapper.name, amount,
                                     self.full_balance(rapper.week))

    def close_period(self, current_week):
        """Closes the current period. Updates the periods closure week and the
        current expenses and balance.
        Parameters
        ----------
        current_week: int
            The current week's number

        Returns
        -------
        Statement
            The credit card statement
        """
        if not self.is_new_period(current_week):
            raise FlowException('Closing period on the same period: abort')
        balance = self.full_balance(current_week)
        minimum_payment = max(self.minimum_pay_rate * (
            self.balance + self.current_expenses), 0)
        late_fees = 0 if self.payed >= minimum_payment else self.late_fees
        interests = self.balance * self.period_rate
        new_balance = balance + late_fees + interests
        statement = Statement(balance, late_fees, interests, self.payed,
                              minimum_payment, current_week, new_balance)
        self.balance = new_balance if new_balance >= 0 else 0
        self.current_expenses = 0
        self.last_period_week = current_week
        self.payed = 0 if new_balance >= 0 else -new_balance
        return statement

    def is_new_period(self, current_week):
        """Returns if the current week starts a new period

        Parameters
        ----------
        current_week: int
            The current week's number

        Returns
        ----------
        Bool
            True if a new period has been reached
        """
        return self.last_period_week + WeeksInPeriod <= current_week

    @property
    def period_rate(self):
        """The interest rate for a single period (not annual)"""
        return self.interest_rate ** 12

    @property
    def available_limit(self):
        """The available limit to spend"""
        return max(0, self.limit - self.balance - self.current_expenses)

    def change_limit(self, new_limit, rapper):
        """Changes the credit card limit
        Parameters
        ----------
        new_limit: float
            The new credit card's limit
        rapper: Rapper
            The credit card's owner

        Returns
        -------
        Bool
            True if the limit was changed, False otherwise
        """
        if new_limit > self.max_limit(rapper):
            return {'type': 'CardChangeLimit', 'result': False}
        self.limit = new_limit
        return {'type': 'CardChangeLimit', 'result': True}

    def pay(self, amount):
        """Tries to pay something with the credit card
        Parameters
        ----------
        amount: float
            The amount of money to pay

        Returns
        -------
        CreditCard:
            The credit card
        """
        if self.available_limit < amount:
            raise InsufficientLimitException()
        self.current_expenses += amount
        return self

    @staticmethod
    def max_limit(rapper):
        """Returns the maximum limit that will be given to a rapper.

        Parameters
        ----------
        rapper: Rapper
            The rapper the limit has to be evaluated for.

        Returns
        -------
        int:
            the maximum limit
        """
        return 10 * rapper.fans

    def as_dict(self):
        next_period_week = self.last_period_week + WeeksInPeriod
        return {
            'interest_rate': self.interest_rate,
            'minimum_pay_rate': self.minimum_pay_rate,
            'limit': self.limit,
            'balance': self.balance,
            'current_expenses': self.current_expenses,
            'late_fees_charge': self.late_fees,
            'last_closure_week': self.last_period_week,
            'next_closure_week': next_period_week,
            'next_closure_minimum_pay': (
                max(self.full_balance(next_period_week) *
                    self.minimum_pay_rate, 0)),
            'next_closure_balance': max(self.full_balance(next_period_week),
                                        0),
            'available_limit': self.available_limit,
            'payed': self.payed
            }

    def _data(self):
        return (self.interest_rate, self.minimum_pay_rate, self.limit,
                self.balance, self.current_expenses, self.late_fees,
                self.last_period_week)

    __eq__ = eq_class
    __repr__ = repr_class


class Statement:

    def __init__(self, balance, late_fees, interests, payed, minimum_payment,
                 week, new_balance):
        self.balance = balance
        self.late_fees = late_fees
        self.interests = interests
        self.payed = payed
        self.minimum_payment = minimum_payment
        self.week = week
        self.new_balance = new_balance

    def as_dict(self):
        return {'balance': self.balance, 'late_fees': self.late_fees,
                'interests': self.interests, 'payed': self.payed,
                'minimum_payment': self.minimum_payment, 'week': self.week,
                'new_balance': self.new_balance}

    def _data(self):
        return (self.balance, self.late_fees, self.interests, self.payed,
                self.minimum_payment, self.week, self.new_balance)

    __hash__ = hash_class
    __repr__ = repr_class


class CreditCardPaymentNote:

    def __init__(self, owner_name, amount, remaining_debt):
        self.owner_name = owner_name
        self.amount = amount
        self.remaining_debt = remaining_debt

    def _data(self):
        return self.owner_name, self.amount, self.remaining_debt

    __hash__ = hash_class
    __repr__ = repr_class
