def as_string(obj):
    """Returns a string representation of an object."""
    return f"'{obj}'" if isinstance(obj, str) else str(obj)


def recursive_round(obj, ndigits=2):
    """Rounds all the floating points numbers in obj."""
    if isinstance(obj, dict):
        return {k: recursive_round(v, ndigits) for k, v in obj.items()}
    elif isinstance(obj, list):
        return [recursive_round(x, ndigits) for x in obj]
    elif isinstance(obj, tuple):
        return tuple([recursive_round(x, ndigits) for x in obj])
    elif isinstance(obj, float):
        return round(obj, ndigits)
    else:
        return obj


def repr_class(self):
    """Returns the repr of an object's instance as long as it has _data()"""
    return '%s(%s)' % (self.__class__.__name__,
                       ', '.join(map(as_string, self._data())))


def hash_class(self):
    """Returns the hash of an object's instance as long as it has _data()"""
    return hash(self._data())


def eq_class(self, other):
    """Compares equality of two object's instance using _data()"""
    return self._data() == other._data()


class Bundle(dict):

    __slots__ = ()

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            return getattr(super(), name)

    def __setattr__(self, name, value):
        self[name] = value

    def __hasattr__(self, name):
        return name in self
