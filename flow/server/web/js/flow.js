var app = new Vue({
  el: '#app',

  data: {
    base_url: location.href,
    user_email: null,
    register: {email: '', name: '', style: 'Lyric'},
    store: [],
    venues: [],
    investments: [],
    rapper: {'cash': 0,
             'certificates_of_deposit': [],
             'credit_card': {'available_limit': 0,
                             'balance': 0,
                             'current_expenses': 0,
                             'interest_rate': 0,
                             'last_closure_week': 0,
                             'late_fees_charge': 0,
                             'limit': 0,
                             'minimum_pay_rate': 0,
                             'next_closure_balance': 0,
                             'next_closure_minimum_pay': 0,
                             'next_closure_week': 4},
             'email': null,
             'fans': 0,
             'flow': 0,
             'investments': [],
             'loans': [],
             'name': '',
             'style': 0,
             'wallet': {'cash': 0},
             'week': 0},
    actions: {command: 'buy_item',
              buy_item: {action: 'buy_item', payment_method: 'cash', item_id: 'Gorra'},
              concert: {action: 'concert', venue_id: "Charo", win: true},
              make_investment: {action: 'make_investment', investment_type: "DiscographicRecord"},
              pay_credit_card: {action: 'pay_credit_card', amount: 0},
              pay_loan: {action: 'pay_loan', amount: 0, loan_id: null},
              relax: {action: 'relax'},
              request_loan: {action: 'request_loan', amount: 10000, interest_rate: 0.5, duration: 10},
              request_new_card_limit: {action: 'request_new_card_limit', new_limit: 10000},
              request_plazo_fijo: {action: 'request_plazo_fijo', amount: 1000, interest_rate: 0.4, duration: 4},
              },
    manager: {result: null, advice: [], updates: [], error: null},
  },

  methods : {

    submit_action() {
        msg = 'Submitting command "' + app.actions.command + '"'
        console.log(msg)
        app['actions'][app.actions.command]['email'] = app.rapper.email
        axios.post(this.base_url + 'action', app.actions[app.actions.command])
          .then(function (response) {
            console.log(msg + ': done')
            console.log(response.data)
            if (response.data.error) {
              app['manager']['error'] = response.data.error
              app['manager']['result'] = null
              app['manager']['advice'] = []
              app['manager']['updates'] = []
            }
            else {
              console.log(response.data)
              app['manager']['result'] = response.data.result
              app['manager']['advice'] = response.data.advice
              app['manager']['updates'] = response.data.updates
              app['rapper'] = response.data.rapper
              app['manager']['error'] = null
            }
          })
          .catch(function (error) {
            console.log(error);
          })
    },

    load_stuff(endpoint, varname) {
        msg = 'Requesting: ' + endpoint
        console.log(msg)
        axios.get(this.base_url + endpoint)
          .then(function (response) {
            console.log(msg + ': done')
            app[varname] = response.data
          })
          .catch(function (error) {
            console.log(error);
          })
    },

    get_rapper(email) {
        endpoint = 'rapper/' + email
        msg = 'Requesting: ' + endpoint
        console.log(msg)
        axios.get(this.base_url + endpoint)
          .then(function (response) {
            console.log(msg + ': done')
            app['rapper'] = response.data.rapper
              console.log(response.data)
          })
          .catch(function (error) {
            console.log(error);
          })
    },

    new_user() {
        msg = 'Registering'
        console.log(msg)
        axios.post(this.base_url + 'rapper', app.register)
          .then(function (response) {
            console.log(msg + ': done')
            app['rapper'] = response.data.rapper
            app['manager']['advice'] = response.data.advice
          })
          .catch(function (error) {
            console.log(error);
          })
    },

  },

  created: function(){
      this.load_stuff('store', 'store')
      this.load_stuff('venues', 'venues')
      this.load_stuff('investments', 'investments')
  },

});
