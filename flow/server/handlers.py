import csv
import json
import logging
import random
import shelve
import time

from operator import attrgetter

from tornado.escape import json_decode
from tornado.autoreload import add_reload_hook

import flow.style

from backend.handlers import MainHandler
from flow.concert import Venue
from flow.exceptions import FlowException
from flow.investment import investment_types
from flow.manager import Manager
from flow.rapper import Rapper
from flow.store import Item, Store
from flow.utils import recursive_round
from flow.world import World


logger = logging.getLogger(__name__)
rappers = world = last_sync = words = None  # Initialize
manager = Manager()


def initialize(rappers_path, store_path, venues_path, words_path):
    global rappers, world, last_sync, words
    # Load rappers
    if rappers is None:
        rappers = shelve.open(rappers_path, writeback=True)
        add_reload_hook(lambda: rappers.close())
        last_sync = time.time()
    if world is not None:
        return
    # Load words for mini games
    with open(words_path, 'r') as f:
        words = json.load(f)
    # Load items
    items = []
    numeric_vars = ['price', 'flow']
    with open(store_path, 'r') as f:
        csv_reader = csv.DictReader(f)
        for item_args in csv_reader:
            for numeric_var in numeric_vars:
                item_args[numeric_var] = int(item_args[numeric_var])
            items.append(Item(**item_args))
    store = Store(items)
    # Load venues
    venues = {}
    numeric_vars = ['cost', 'capacity', 'ticket_price']
    with open(venues_path, 'r') as f:
        csv_reader = csv.DictReader(f)
        for venue_args in csv_reader:
            for numeric_var in numeric_vars:
                venue_args[numeric_var] = int(venue_args[numeric_var])
            venues[venue_args['name']] = Venue(**venue_args)
    # Build world
    world = World(venues, store)


def save():
    global last_sync
    if rappers is not None and time.time() > last_sync + 300:
        rappers.sync()
        last_sync = time.time()


def close():
    if rappers is not None:
        rappers.close()


class InvestmentsHandler(MainHandler):

    def get(self):
        """Returns the investment posibilities."""
        investments_dicts = [cls.as_dict_cls() for cls in
                             investment_types.values()]
        investments = manager.translate_investments(investments_dicts)
        investments = sorted(investments, key=lambda x: len(x['nice_name']))
        self.write_json(investments)


class LoginHandler(MainHandler):

    def post(self, rapper_email):
        """Validates the user password."""
        try:
            rapper = rappers[rapper_email]
            user_pass = getattr(rapper, 'password',
                                rapper.extra_info.get('password'))
            provided_pass = json_decode(self.request.body).get('password')
            if provided_pass == user_pass:
                self.write_json(dict(ok=1))
            else:
                self.write_json(dict(ok=0))
        except KeyError:
            self.write_json(dict(ok=0))


class RankingHandler(MainHandler):

    def get(self, rapper_email):
        """Returns an existing rapper corresponding to its email."""
        rapper_list = list(rappers.values())
        fans = sorted(rapper_list, key=attrgetter('fans'), reverse=True)
        flow = sorted(rapper_list, key=attrgetter('flow'), reverse=True)
        money = sorted(rapper_list, key=attrgetter('net_worth'), reverse=True)
        rankings = {'fans': list(map(self.fans_getter, fans))[:10],
                    'flow': list(map(self.flow_getter, flow))[:10],
                    'money': list(map(self.money_getter, money))[:10]}
        if rapper_email:
            rankings['rapper'] = {
                'fans': self.rapper_index(rapper_email, fans),
                'money': self.rapper_index(rapper_email, money),
                'flow': self.rapper_index(rapper_email, flow)}
            rankings['rappers'] = len(rapper_list)
        rankings = recursive_round(rankings)
        self.write_json(rankings)

    @staticmethod
    def fans_getter(rapper):
        return {'name': rapper.name, 'value': rapper.fans}

    @staticmethod
    def flow_getter(rapper):
        return {'name': rapper.name,
                'value': (round(rapper.flow) if rapper.flow == rapper.flow else
                          'Flow master')}

    @staticmethod
    def money_getter(rapper):
        net_worth = rapper.net_worth
        return {'name': rapper.name,
                'value': (round(net_worth) if net_worth == net_worth else
                          'Mega $$$$ master')}

    @staticmethod
    def rapper_index(rapper_email, rappers_list):
        return next(i + 1 for i, rapper in enumerate(rappers_list)
                    if rapper.email == rapper_email)


class RapperHandler(MainHandler):

    def get(self, rapper_email):
        """Returns an existing rapper corresponding to its email."""
        rapper = recursive_round(rappers[rapper_email].as_dict())
        ret = manager.empty_msg()
        ret['rapper'] = rapper
        self.write_json(ret)

    def post(self, rapper_email):
        """Creates a new rapper.

        Parameters
        ----------
        name: str
        style: flow.styles

        Returns
        -------
        rapper
        """
        kwargs = json_decode(self.request.body)
        email = kwargs.pop('email')
        # Against bots
        email = email.replace('.php', '_php').replace('.cgi', '_cgi')
        if email in rappers:
            self.write_error(400, message='Email already registered')
            return
        name = kwargs.pop('name')
        # Against bots
        name = name.replace('.php', '_php').replace('.cgi', '_cgi')
        style = getattr(flow.style, kwargs.pop('style'))
        rapper = Rapper(email, name, style, flow=49, fans=17, week=1, **kwargs)
        rappers[email] = rapper
        rapper = recursive_round(rapper.as_dict())
        ret = manager.empty_msg()
        ret['rapper'] = rapper
        ret['advice'] = manager.welcome(rapper)
        self.write_json(ret)
        save()

    def delete(self, rapper_email):
        """Creates a new rapper.

        Parameters
        ----------
        name: str
        style: flow.styles

        Returns
        -------
        rapper
        """
        try:
            rapper = rappers[rapper_email]
            user_pass = getattr(rapper, 'password',
                                rapper.extra_info.get('password'))
            provided_pass = json_decode(self.request.body).get('password')
            if provided_pass == user_pass:
                del rappers[rapper_email]
                self.write_json(dict(ok=1))
            else:
                self.write_json(dict(ok=0))
        except KeyError:
            self.write_json(dict(message='That email is not registered'))


class StoreHandler(MainHandler):

    def get(self):
        """Returns the store's items for sale."""
        self.write_json(world.store.as_dict())


class TipHandler(MainHandler):

    def get(self, rapper_email):
        """Returns the tips."""
        rapper = rappers[rapper_email] if rapper_email else None
        tips = manager.tell_me_tips(rapper)
        self.write_json(tips)


class VenueHandler(MainHandler):

    def get(self):
        """Returns the venues."""
        self.write_json([venue.as_dict() for venue in world.venues.values()])


class WordHandler(MainHandler):

    def get(self, lang, n):
        """Returns an amount of random words."""
        try:
            w = random.sample(words[lang], int(n))
            self.write_json(w)
        except Exception:
            self.write_json(['Hello', 'Hola', 'Bonjour', 'Alo'])


class WorldHandler(MainHandler):

    def post(self):
        """Process an action for a rapper.

        Parameters
        ----------
        email: str
            The rapper's email
        action: str
            The action to perform
        other_args: extra arguments for the specific action

        Returns
        -------
        The update, see Manager.work
        """
        kwargs = json_decode(self.request.body)
        email = kwargs.pop('email')
        action = kwargs.pop('action')
        rapper = rappers[email]
        try:
            result, updates = world.act(rapper, action, **kwargs)
            ret = manager.work(rapper, action, result, updates)
            ret = recursive_round(ret)
            self.write_json(ret)
            save()
        except FlowException as e:
            if type(e) == FlowException:
                logger.error('Error in action: %s', str(kwargs), exc_info=True)
            ret = manager.explain_exception(e)
            self.write_json(ret)


class LogHandler(MainHandler):

    valid_words = ('/action', '/investments', '/login', '/ranking', '/rapper',
                   '/store', '/tips', '/venues', '/words', '/verlog')

    def get(self, secret, n):
        if not isinstance(secret, str):
            return
        secret_hash = 5381
        for c in secret:
            c = ord(c)
            secret_hash = ((secret_hash << 5) + secret_hash) + c
        if secret_hash != 8980556474551667107148:
            return
        with open('/tmp/flow.log', 'r') as f:
            lines = [line for line in f
                     if any(w in line for w in self.valid_words)]
        if n:
            n = int(n)
            lines = lines[-n:]
        lines = ''.join(lines)
        self.set_header("Content-Type", "text/plain")
        self.content_type = 'text/plain'
        self.write(lines)
