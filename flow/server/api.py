import os.path

import tornado.web

from backend.utils import get_package_path
from flow.server.handlers import (InvestmentsHandler, LoginHandler, LogHandler,
                                  RankingHandler, RapperHandler, StoreHandler,
                                  TipHandler, VenueHandler, WordHandler,
                                  WorldHandler)


static_path = os.path.join(get_package_path(), 'flow', 'server', 'web')


def make_app(app_settings):
    return tornado.web.Application([
        (r'/action', WorldHandler),
        (r'/investments', InvestmentsHandler),
        (r'/login/(.*)', LoginHandler),
        (r'/ranking/?(.*)', RankingHandler),
        (r'/rapper/?(.*)', RapperHandler),
        (r'/store', StoreHandler),
        (r'/tips/?(.*)', TipHandler),
        (r'/venues', VenueHandler),
        (r'/words/(.+)/(\d+)', WordHandler),
        (r'/verlog/(.*?)/?(\d*)', LogHandler),
        (r'/(.*)', tornado.web.StaticFileHandler,
         dict(path=static_path, default_filename='index.html')),
     ], **app_settings)
