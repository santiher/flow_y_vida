#!/usr/bin/env python
# vim: ft=python

from backend.server import run_server
from backend.utils import load_config_and_logging, get_package_path

from flow.server.api import make_app
from flow.server.handlers import initialize, close


def main():
    config_files = ['%s/backend/default.cfg' % get_package_path(),
                    '%s/flow/server/default.cfg' % get_package_path()]
    config = load_config_and_logging(config_files)
    initialize(config.rappers, config.store, config.venues, config.words)
    run_server(make_app, config.app, config.fork_method, config.single_process,
               config.port, config.max_wait, config.ssl)
    close()


if __name__ == "__main__":
    main()
