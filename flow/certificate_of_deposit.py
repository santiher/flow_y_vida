"""Plazo fijo"""

from flow.constants import CashPayment, YearWeeks
from flow.utils import repr_class


class CertificateOfDepositStatement:

    def __init__(self, invested, payout, duration, start_week, interest_rate):
        self.invested = invested
        self.payout = payout
        self.duration = duration
        self.start_week = start_week
        self.interest_rate = interest_rate

    def as_dict(self):
        return {'invested': self.invested, 'interest_rate': self.interest_rate,
                'duration': self.duration, 'start_week': self.start_week,
                'payout': self.payout}

    def _data(self):
        return (self.invested, self.interest_rate, self.duration,
                self.start_week, self.interest_rate)

    __repr__ = repr_class


class CertificateOfDeposit:

    def __init__(self, money, interest_rate, duration, start_week):
        """A plazo fijo deposit

        Parameters
        ----------
        money: float
            The amount of money requested.
        interest_rate: float
            The CD interest rate.
        duration: int
            The CD duration in weeks.
        start_week: int
            The start week
        """
        self.money = money
        self.interest_rate = interest_rate
        self.duration = duration
        self.start_week = start_week

    @property
    def payout(self):
        """The amount of money you get back"""
        rate = (1 + self.interest_rate) ** (self.duration / YearWeeks)
        return self.money * rate

    def check(self, rapper):
        """Checks if the plazo fijo finished, in case it did it returns a
        Finished object or None. And returns the money to the rapper.

        Parameters
        ----------
        rapper: Rapper

        Returns
        -------
        CertificateOfDepositStatement / None
            A statement if it finished or None
        """
        if rapper.week >= self.start_week + self.duration:
            rapper.pay(self.payout)
            return CertificateOfDepositStatement(
                self.money, self.payout, self.duration, self.start_week,
                self.interest_rate)

    @staticmethod
    def request(rapper, amount, interest_rate, duration):
        """Request a new CD. This should be the way to generate them.
        It will be generated and added to the rapper

        Parameters
        ----------
        rapper: Rapper
            The rapper requesting the CD.
        amount: float
            The amount of money offered.
        interest_rate: float
            The CD interest rate.
        duration: int
            The CD duration in weeks.

        Returns
        -------
        CD
        """
        cd = CertificateOfDeposit(amount, interest_rate, duration, rapper.week)
        rapper.add_certificate_of_deposit(cd)
        rapper.charge(amount, CashPayment)
        return cd

    def as_dict(self):
        return {'money': self.money, 'interest_rate': self.interest_rate,
                'duration': self.duration, 'start_week': self.start_week}

    def _data(self):
        return self.money, self.interest_rate, self.duration, self.start_week

    __repr__ = repr_class
