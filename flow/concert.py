"""This module contains the code to handle concerts"""

from math import log, sin

from flow.utils import hash_class, repr_class


class Venue:

    def __init__(self, name, cost, capacity, style, ticket_price,
                 **extra_info):
        """A venue is a place where a rapper can give a concert.

        Parameters
        ----------
        name: str
            The venue's name
        cost: number
            Value of renting the venue to give a concert
        capacity: int
            The maximum amount of people that can attend a concert here
        style: Style
            The preferred rapper style in this venue
        ticket_price: number
            How much each ticket cost
        extra_info: kwargs dict
            Any extra variables that wants to be stored, not related to the
            rules. This variables will be forwarded to the as_dict method.
            Eg: image url.
        """
        self.name = name
        self.cost = cost
        self.capacity = capacity
        self.style = style
        self.ticket_price = ticket_price
        self.extra_info = extra_info

    def concert(self, rapper, win):
        """Give a concert in the venue.
        After the concert, the rapper is charged (money and flow) and payed.

        Parameters
        ----------
        rapper: Rapper
            The rapper that will give the concert, must have enough funds
        win: boolean
            True: The rapper won the concert and gets a good amount of fans
            False: the rapper performed poorly on the battle and gets less fans

        Returns
        -------
        Concert
            A summary of the concert.
        """
        attendants = self._attendants(rapper)
        earnings = self.ticket_price * attendants
        new_fans = self._fanatized(attendants, rapper, win)
        rapper.charge(self.cost, 'cash')
        used_flow = rapper.perform_for(attendants)
        rapper.pay(earnings)
        if new_fans:
            rapper.acknowledge_new_fans(new_fans)
        concert = Concert(rapper, self, attendants, new_fans, earnings,
                          used_flow)
        return concert

    def _bonus(self, rapper):
        """Determines a bonus factor for the result"""
        return 1.04 if rapper.style == self.style else 1

    def _attendants(self, rapper):
        """Determines how many dudes attended the concert"""
        x = rapper.fans
        attendants = int((log(x + 1) * sin(x ** 0.5) * x ** 0.5 +
                          0.7 * x +
                          8 * x ** 0.5 +
                          0.5 * x ** 1.05) * self._bonus(rapper))
        return min(attendants, self.capacity)

    def _fanatized(self, attendants, rapper, win):
        """Determines how many dudes turned into fans
        attendants: int
            The amount of attendants
        rapper: Rapper
            The rapper that gave the concert
        win: boolean
            True: The rapper won the concert and gets a good amount of fans
            False: the rapper performed poorly on the battle and gets less fans

        Returns
        -------
        int:
            The amount of new fans
        """
        if not win:
            return 0
        x = attendants - rapper.fans
        base = 0.08 * x
        if base <= 10:
            base = 12 + rapper.fans % 8
        if x < 0:
            x = 0
        newbie_boost = 0 if rapper.fans > 1000 else 1.05 ** x
        middle_boost = 0 if 1000 < rapper.fans < 4000 else 0.003 * x ** 1.003
        new_fans = int(base + newbie_boost + middle_boost)
        if new_fans % 8 == 0:
            new_fans = int(new_fans * 1.02)
        if new_fans % 3 == 0:
            new_fans = int(new_fans * 0.97)
        new_fans = min(new_fans, self.capacity)
        return new_fans

    def as_dict(self):
        return {**self.extra_info, 'name': self.name, 'cost': self.cost,
                'capacity': self.capacity}

    def _data(self):
        return (self.name, self.cost, self.capacity, self.style,
                self.ticket_price)

    __hash__ = hash_class
    __repr__ = repr_class


class Concert:

    def __init__(self, rapper, venue, attendants, new_fans, earnings,
                 used_flow):
        """A concert is the representation of a rapper's performancec in a
        venue.
        Parameters
        ----------
        rapper: Rapper
            The rapper that gave the concert
        venue: Venue
            The venue where the concert was given
        attendants: int
            The amount of people that went to the concert
        new_fans: int
            The amount of people that turned into new fans
        earnings: number
            How much money was gather from ticket sales
        used_flow: int
            How much flow the rapper lost / gained
        """
        self.artist = rapper.name
        self.venue = venue.name
        self.attendants = attendants
        self.new_fans = new_fans
        self.earnings = earnings
        self.used_flow = used_flow
        self.net_revenue = earnings - venue.cost

    def as_dict(self):
        return {'artist': self.artist, 'venue': self.venue,
                'attendants': self.attendants, 'new_fans': self.new_fans,
                'earnings': self.earnings, 'net_revenue': self.net_revenue,
                'used_flow': self.used_flow}

    def _data(self):
        return (self.artist, self.venue, self.attendants, self.new_fans,
                self.earnings, self.used_flow, self.net_revenue)

    __hash__ = hash_class
    __repr__ = repr_class
