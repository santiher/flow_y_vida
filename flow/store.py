"""This module contains a store"""


from flow.exceptions import (ItemDoesNotExistException,
                             InsufficientMoneyException,
                             InsufficientMoneyAtStoreException)
from flow.utils import hash_class, repr_class, eq_class


class Item:

    def __init__(self, name, price, flow, **extra_info):
        """A general item that can be bought by the rapper.

        Parameters
        ----------
        name: str
            The item's name / id
        price: float
            The item's price
        flow: int
            The amount of flow it provides to the rapper.
        extra_info: kwargs dict
            Any extra variables that wants to be stored, not related to the
            rules. This variables will be forwarded to the as_dict method.
            Eg: image url.
        """
        self.name = name
        self.price = price
        self.flow = flow
        self.extra_info = extra_info

    def as_dict(self):
        return {**self.extra_info, 'name': self.name, 'price': self.price,
                'flow': self.flow}

    def _data(self):
        return self.name, self.price, self.flow

    __eq__ = eq_class
    __hash__ = hash_class
    __repr__ = repr_class


class Store:

    def __init__(self, items):
        """A store which contains items that can be bought, which provide flow.

        Parameters
        ----------
        items: [Item]
            The items available for sale
        """
        self.items = items
        self.items_table = {item.name: item for item in items}

    def sell(self, rapper, item_id, payment_method):
        """Sell an item to the rapper, charge money and give the item.

        Parameters
        ----------
        rapper: Rapper
            The rapper whom the store is selling to
        item_id: int/str
            The id of the item to sell
        payment_method: constant
            The payment method, one in constans.py

        Returns
        -------
        item: item
        """
        try:
            item = self.items_table[item_id]
        except KeyError:
            raise ItemDoesNotExistException(item_id)
        try:
            rapper.charge(item.price, payment_method)
        except InsufficientMoneyException:
            raise InsufficientMoneyAtStoreException
        return PurchaseTicket(rapper.name, item.name, item.price, item.flow,
                              payment_method)

    def as_dict(self):
        return {'items': [item.as_dict() for item in self.items]}

    def _data(self):
        return self.items

    __hash__ = hash_class
    __repr__ = repr_class


class PurchaseTicket:

    def __init__(self, buyer_name, item_name, price, flow, payment_method):
        """A purchase ticket.

        Parameters
        ----------
        buyer_name: str
            The buyers's name
        item_name: str
            The item's name / id
        price: float
            The item's price
        flow: int
            The amount of flow it provides to the rapper.
        payment_method: constants.PaymentMethod
            How the purchase was paid
        """
        self.buyer_name = buyer_name
        self.item_name = item_name
        self.price = price
        self.flow = flow
        self.payment_method = payment_method

    def _data(self):
        return (self.buyer_name, self.item_name, self.price, self.flow,
                self.payment_method)

    __eq__ = eq_class
    __repr__ = repr_class
