from random import random


def tip_0(rapper, action, action_result):
    """
    Parameters
    ----------
    rapper: Rapper
    action: str
    action_result: Document

    Result
    ------
    bool
        Returns true if the tip applies to the rapper in this situation
    """
    return action == 'request_loan'


tip_1 = tip_2 = tip_3 = tip_4 = tip_0


def tip_5(rapper, action, action_result):
    return (action == 'concert' and action_result.net_revenue < 20000 and
            random() < 0.33)


def tip_6(rapper, action, action_result):
    return (action == 'concert' and action_result.net_revenue < 20000 and
            random() < 0.33)


def tip_7(rapper, action, action_result):
    return action == 'request_plazo_fijo'


tip_10 = tip_9 = tip_8 = tip_7


def tip_11(rapper, action, action_result):
    return action == 'buy_item' and random() < 0.2


def tip_12(rapper, action, action_result):
    return action == 'make_investment'


def tip_13(rapper, action, action_result):
    return action == 'make_investment'


def tip_14(rapper, action, action_result):
    return rapper.wallet.cash > 300 and random() < 0.1


def tip_15(rapper, action, action_result):
    return rapper.wallet.cash > 400 and random() < 0.1


def tip_16(rapper, action, action_result):
    return rapper.wallet.cash > 3000 and random() < 0.1


def tip_17(rapper, action, action_result):
    return (action in ('make_investment', 'request_plazo_fijo') and
            random() < 0.1)


def tip_18(rapper, action, action_result):
    return action == 'concert' and random() < 0.05


def tip_19(rapper, action, action_result):
    return (action in ('make_investment', 'request_plazo_fijo') and
            random() < 0.1)


tip_21 = tip_20 = tip_19


def tip_22(rapper, action, action_result):
    return (action in ('request_load', 'request_plazo_fijo') and
            random() < 0.05)


tip_26 = tip_25 = tip_23 = tip_22


def tip_27(rapper, action, action_result):
    return random() < 0.1


def tip_28(rapper, action, action_result):
    return random() < 0.05


def tip_29(rapper, action, action_result):
    return len([l for l in rapper.loans
                if l.start_week + l.duration > rapper.week]) > 2


def tip_30(rapper, action, action_result):
    return (action == 'buy_item' and action_result.payment_method == 'credit'
            and random() < 0.2)


def tip_31(rapper, action, action_result):
    return action == 'buy_item' and action_result.payment_method == 'credit'


def tip_32(rapper, action, action_result):
    return (action == 'buy_item' and action_result.payment_method == 'credit'
            and random() < 0.1)


tip_34 = tip_33 = tip_32


def tip_35(rapper, action, action_result):
    return (action == 'concert' and random() < 0.1)


def tip_36(rapper, action, action_result):
    return (action == 'relax' and random() < 0.2)


def tip_37(rapper, action, action_result):
    return (action == 'concert' and random() < 0.05)


def tip_38(rapper, action, action_result):
    return (action == 'buy_item' and random() < 0.15)


def tip_39(rapper, action, action_result):
    return (action == 'concert' and action_result.used_flow > rapper.flow and
            random() < 0.45)


def tip_40(rapper, action, action_result):
    return (action == 'concert' and random() < 0.15)


# Extras


only_once = {i for i in range(41)}


def not_seen(tip_number, tip):
    if tip_number not in only_once:
        return tip

    def new_tip(rapper, action, action_result):
        return (tip_number not in rapper.seen_tips and
                tip(rapper, action, action_result))

    return new_tip


def is_tip(varname):
    return varname.startswith('tip_')


def number(varname):
    return int(varname.split('_')[1])


tips = [(number(varname), not_seen(number(varname), value))
        for varname, value in globals().items() if is_tip(varname)]
tips = sorted(tips, key=lambda x: x[0])
