class FlowException(Exception):
    pass


class InsufficientMoneyException(FlowException):
    def __init__(self, *args):
        super().__init__("You don't have enough money for that")


class InsufficientMoneyAtStoreException(FlowException):
    def __init__(self, *args):
        super().__init__("You don't have enough money for that")


class InsufficientLimitException(FlowException):
    def __init__(self, *args):
        super().__init__("You don't have enough limit to buy that")


class InsufficientFlowException(FlowException):
    def __init__(self, *args):
        super().__init__("You don't have enough flow to do that")


class ItemDoesNotExistException(FlowException):
    def __init__(self, *args):
        super().__init__("What are you trying to buy dude?")


class LoanNotGrantedException(FlowException):
    pass


class LoanNotFoundException(FlowException):
    pass


class TooRelaxedException(FlowException):
    pass
