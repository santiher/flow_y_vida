import os
import re
import time


cmd = 'sudo iptables -A INPUT -s %s -j DROP'
regex = r'(\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d) - .*? (\S+) \((\d[^\)]+)\)'
regex = re.compile(regex)


def ban_ips(log_path, start_time, banned_ips):
    with open(log_path, 'r') as f:
        for line in f:
            match = regex.match(line)
            if match:
                dt, link, ip = match.groups()
                if dt >= start_time:
                    if '.cgi' in link or '.php' in link:
                        if ip not in banned_ips:
                            banned_ips.add(ip)
                            os.system(cmd % ip)
    return start_time


def ban_police(log_path, start_time='2000-01-01 10:00:00', every=600):
    banned_ips = set()
    while True:
        try:
            start_time = ban_ips(log_path, start_time, banned_ips)
            if len(banned_ips) > 10000:
                banned_ips = set()
            time.sleep(every)
        except KeyboardInterrupt:
            break


if __name__ == '__main__':
    log_path = '/tmp/flow.log'
    ban_police(log_path)
