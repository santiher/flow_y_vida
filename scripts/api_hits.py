import json
import pprint
import urllib.parse
import urllib.request


def hit(data):
    endpoint = data.pop('endpoint')
    method = data.pop('method', None)
    url = 'http://localhost:8888/' + endpoint
    req = None
    if data:
        data = json.dumps(data).encode('utf8')
        header = {'Content-Type': 'application/json'}
        req = urllib.request.Request(url, data, header)
    else:
        req = urllib.request.Request(url)
    if method:
        req.get_method = lambda: method
    ret = urllib.request.urlopen(req)
    response = ret.read()
    if response:
        response = json.loads(response.decode('utf-8'))
    ret.close()
    return response


def hit_all():
    pp = pprint.PrettyPrinter(indent=4)
    for data in hits:
        print('----------')
        print(data)
        response = hit(data)
        pp.pprint(response)


hits = [
    {"endpoint": "rapper/sugar@magic.com", "method": "DELETE", "password": "hola"},
    {"endpoint": "store"},
    {"endpoint": "investments"},
    {"endpoint": "venues"},
    {"endpoint": "rapper", "name": "Sugar Magic", "email": "sugar@magic.com", "style": "Aggresive", "password": "hola"},
    {"endpoint": "ranking"},
    {"endpoint": "ranking/sugar@magic.com"},
    {"endpoint": "tips"},
    {"endpoint": "tips/sugar@magic.com"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "request_loan", "amount": 10000, "interest_rate": 0.18, "duration": 10},
    {"endpoint": "rapper/sugar@magic.com"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "concert", "venue_id": "Charo", "win": True},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "request_plazo_fijo", "amount": 100000, "interest_rate": 0.3, "duration": 4},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "make_investment", "investment_type": "DiscographicRecord"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "buy_item", "item_id": "Lentes", "payment_method": "credit"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "buy_item", "item_id": "Gorra", "payment_method": "cash"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "pay_credit_card", "amount": 100},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "pay_loan", "amount": 24, "loan_id": 123013},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "make_investment", "investment_type": "FashionBrand"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "request_new_card_limit", "new_limit": 500},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "make_investment", "investment_type": "VideoProductor"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "concert", "venue_id": "Charo", "win": False},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "pay_credit_card", "amount": 600},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "action", "email": "sugar@magic.com", "action": "relax"},
    {"endpoint": "rapper/sugar@magic.com", "method": "DELETE", "password": "hola"},
    ]


hit_all()
