import csv

from flow.constants import CashPayment, CreditPayment
from flow.concert import Venue
from flow.investment import DiscographicRecord
from flow.manager import Manager
from flow.rapper import Rapper
from flow.store import Item, Store
from flow.style import Lyric
from flow.wallet import Wallet
from flow.world import World


world, rappers = None, dict()


def initialize(store_path, venues_path):
    global rappers, world

    def create_ray():
        rapper_kwargs = dict(email='ray@flow.com', name='Ray Jones', fans=3200,
                             style=Lyric, flow=50, week=0,
                             wallet=Wallet(50000))
        rapper = Rapper(**rapper_kwargs)
        rapper.add_investment(DiscographicRecord(start_week=0))
        rappers[rapper.email] = rapper

    create_ray()
    # Load items
    items = []
    numeric_vars = ['price', 'flow']
    with open(store_path, 'r') as f:
        csv_reader = csv.DictReader(f)
        for item_args in csv_reader:
            for numeric_var in numeric_vars:
                item_args[numeric_var] = int(item_args[numeric_var])
            items.append(Item(**item_args))
    store = Store(items)
    # Load venues
    venues = {}
    numeric_vars = ['cost', 'capacity', 'ticket_price']
    with open(venues_path, 'r') as f:
        csv_reader = csv.DictReader(f)
        for venue_args in csv_reader:
            for numeric_var in numeric_vars:
                venue_args[numeric_var] = int(venue_args[numeric_var])
            venues[venue_args['name']] = Venue(**venue_args)
    # Build world
    world = World(venues, store)


def do(rapper, action, *args, show_manager=True,
       keys=['result', 'updates', 'advice'], not_implemented=True):
    print('-----------------')
    try:
        ret, updates = world.act(rapper, action, *args)
        print(ret)
        if show_manager:
            msg = manager.work(rapper, action, ret, updates)
            for key in keys:
                if msg[key]:
                    print(msg[key])
    except NotImplementedError:
        print('%s: not fully implemented' % action)
        if not not_implemented:
            raise


if __name__ == '__main__':
    initialize('../data/store.csv', '../data/venues.csv')
    manager = Manager()
    ray = rappers['ray@flow.com']
    do(ray, 'buy_item', 'Gorra', CashPayment)
    do(ray, 'buy_item', 'Gorra', CreditPayment)
    do(ray, 'concert', 'BitFlow', True)
    do(ray, 'concert', 'BitFlow', False)
    do(ray, 'make_investment', 'DiscographicRecord')
    do(ray, 'pay_credit_card', 200)
    do(ray, 'relax')
    do(ray, 'request_loan', 1000, 0.14, 4)
    do(ray, 'pay_loan', 200, ray.loans[0].id)
    do(ray, 'request_new_card_limit', 10000)
    do(ray, 'request_plazo_fijo', 100, 0.2, 4)
    do(ray, 'relax')
    do(ray, 'relax')
    do(ray, 'relax')
    do(ray, 'relax')
    do(ray, 'relax')
    do(ray, 'relax')
    do(ray, 'relax')
    do(ray, 'relax')
    do(ray, 'relax')
