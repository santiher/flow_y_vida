#!/bin/bash


# create key and csr
openssl req -x509 -sha256 -newkey rsa:2048 -nodes -keyout server.key -days 365 -out server.crt
# validate
openssl x509 -text -noout -in server.crt

# Method 2
# PASS=123456
# openssl genrsa -des3 -passout pass:$PASS -out server.pass.key 2048
# openssl rsa -passin pass:$PASS -in server.pass.key -out server.key
# rm server.pass.key
# openssl req -new -key server.key -out server.csr
# openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt
