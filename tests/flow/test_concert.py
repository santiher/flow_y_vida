from unittest import TestCase, main
from unittest.mock import MagicMock

from flow.concert import Venue, Concert
from flow.constants import CashPayment
from flow.exceptions import InsufficientMoneyException
from flow.style import Lyric, Melodic
from flow.utils import Bundle


rapper_name, rapper_fans, rapper_style = 'Ray Jones', 3200, Lyric


class TestVenue(TestCase):

    args = name, cost, capacity, style, ticket = 'Charo', 1000, 2000, Lyric, 12

    def test_init(self):
        venue = Venue(*self.args)
        self.assertEqual(venue.name, self.name)
        self.assertEqual(venue.cost, self.cost)
        self.assertEqual(venue.capacity, self.capacity)
        self.assertEqual(venue.style, self.style)
        self.assertEqual(venue.ticket_price, self.ticket)

    def test_as_dict(self):
        expected = {'name': self.name, 'cost': self.cost,
                    'capacity': self.capacity}
        venue = Venue(*self.args)
        self.assertEqual(venue.as_dict(), expected)

    def test_concert_basic(self):
        rapper = MagicMock()
        rapper.name = rapper_name
        rapper.fans = rapper_fans
        rapper.style = rapper_style
        venue = Venue(*self.args)
        concert = venue.concert(rapper)
        self.assertEqual(concert.__class__, Concert)
        rapper.charge.assert_called_once()
        rapper.perform_for.assert_called_once()
        rapper.pay.assert_called_once()
        rapper.acknowledge_new_fans.assert_called_once()
        self.assertIsInstance(rapper.charge.call_args[0][0], (float, int))
        self.assertEqual(rapper.charge.call_args[0][1], CashPayment)
        self.assertIsInstance(rapper.perform_for.call_args[0][0], int)
        self.assertIsInstance(rapper.pay.call_args[0][0], (float, int))
        self.assertIsInstance(rapper.acknowledge_new_fans.call_args[0][0],
                              int)

    def test_concert_cant_pay(self):
        rapper = MagicMock()
        rapper.name = rapper_name
        rapper.fans = rapper_fans
        rapper.style = rapper_style
        rapper.charge.side_effect = InsufficientMoneyException()
        venue = Venue(*self.args)
        with self.assertRaises(InsufficientMoneyException):
            venue.concert(rapper)
        rapper.charge.assert_called_once()
        rapper.perform_for.assert_not_called()
        rapper.pay.assert_not_called()
        rapper.acknowledge_new_fans.assert_not_called()

    def test_bonus(self):
        venue = Venue(*self.args)
        rapper = Bundle(style=self.style)
        bonus = venue._bonus(rapper)
        self.assertTrue(bonus > 1)

    def test_no_bonus(self):
        venue = Venue(*self.args)
        rapper = Bundle(style=Melodic)
        bonus = venue._bonus(rapper)
        self.assertEqual(bonus, 1)

    def test_attendants_limit(self):
        venue = Venue(*self.args)
        rapper = Bundle(style=Melodic, fans=1e6)
        attendants = venue._attendants(rapper)
        self.assertLessEqual(attendants, venue.capacity)

    def test_attendants_positive(self):
        venue = Venue(*self.args)
        for fans in range(100):
            rapper = Bundle(style=Melodic, fans=fans)
            attendants = venue._attendants(rapper)
            self.assertGreaterEqual(attendants, 0)

    def test_attendants_bonus(self):
        venue = Venue(*self.args)
        for fans in [10, 100, 200, 770, 1431]:
            rapper_a = Bundle(style=Lyric, fans=fans)
            rapper_b = Bundle(style=Melodic, fans=fans)
            attendants_a = venue._attendants(rapper_a)
            attendants_b = venue._attendants(rapper_b)
            self.assertGreaterEqual(attendants_a, attendants_b)


class TestConcert(TestCase):

    def test_init(self):
        rapper = Bundle(name=rapper_name)
        venue = Venue(*TestVenue.args)
        attendants, new_fans, earnings = 114, 24, 2048
        concert = Concert(rapper, venue, attendants, new_fans, earnings)
        self.assertEqual(concert.artist, rapper_name)
        self.assertEqual(concert.venue, venue.name)
        self.assertEqual(concert.attendants, attendants)
        self.assertEqual(concert.new_fans, new_fans)
        self.assertEqual(concert.earnings, earnings)
        self.assertEqual(concert.net_revenue, earnings - venue.cost)

    def test_as_dict(self):
        attendants, new_fans, earnings = 114, 24, 2048
        expected = {'artist': rapper_name, 'venue': TestVenue.name,
                    'attendants': attendants, 'new_fans': new_fans,
                    'earnings': earnings,
                    'net_revenue': earnings - TestVenue.cost}
        rapper = Bundle(name=rapper_name)
        venue = Venue(*TestVenue.args)
        concert = Concert(rapper, venue, attendants, new_fans, earnings)
        self.assertEqual(concert.as_dict(), expected)


if __name__ == '__main__':
    main()
