from unittest import TestCase, main
from unittest.mock import MagicMock

from flow.constants import CashPayment
from flow.exceptions import (InsufficientMoneyException,
                             InsufficientLimitException,
                             ItemDoesNotExistException)
from flow.store import Item, Store, PurchaseTicket


class TestStore(TestCase):

    items = Item('a', 4, 1), Item('b', 7, 4), Item('c', 1, 2)
    sorted_items = [Item('c', 1, 2), Item('a', 4, 1), Item('b', 7, 4)]

    def test_init(self):
        store = Store(self.items)
        self.assertEqual(store.items, self.sorted_items)

    def test_as_dict(self):
        expected = dict(items=[i.as_dict() for i in self.sorted_items])
        self.assertEqual(Store(self.items).as_dict(), expected)

    def test_sell_ok(self):
        rapper = MagicMock()
        store = Store(self.items)
        ret = store.sell(rapper, self.items[0].name, CashPayment)
        rapper.charge.assert_called_once_with(self.items[0].price, CashPayment)
        expected = PurchaseTicket(rapper.name, self.items[0].name, 
                                  self.items[0].price, self.items[0].flow,
                                  CashPayment)
        self.assertEqual(ret, expected)

    def test_sell_bad_item_key(self):
        with self.assertRaises(ItemDoesNotExistException):
            raise ItemDoesNotExistException

    def test_sell_rapper_does_not_have_enough_cash(self):
        with self.assertRaises(InsufficientMoneyException):
            raise InsufficientMoneyException

    def test_sell_rapper_does_not_have_enough_limit(self):
        with self.assertRaises(InsufficientLimitException):
            raise InsufficientLimitException


if __name__ == '__main__':
    main()
