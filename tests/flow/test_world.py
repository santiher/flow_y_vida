from unittest import TestCase, main
from unittest.mock import Mock

from flow.concert import Venue
from flow.constants import CashPayment, CreditPayment
from flow.credit_card import CreditCard
from flow.exceptions import (FlowException, InsufficientLimitException,
                             InsufficientMoneyException)
from flow.rapper import Rapper
from flow.store import Store, Item
from flow.style import Melodic
from flow.wallet import Wallet
from flow.world import World


class TestWorld(TestCase):

    @staticmethod
    def _new_rapper(email='ray@flow.com', name='Ray Jones', style=Melodic,
                    flow=120, fans=100, week=0, cash=100,
                    cc_interest_rate=0.14, cc_minimum_pay_rate=0.10,
                    cc_limit=2000, cc_balance=0, cc_current_expenses=0,
                    cc_late_fees=100, cc_last_period_week=0, loans=None):
        card = CreditCard(cc_interest_rate, cc_minimum_pay_rate, cc_limit,
                          cc_balance, cc_current_expenses, cc_late_fees,
                          cc_last_period_week)
        loans = loans if loans is not None else []
        return Rapper(email, name, style, flow, fans, week, Wallet(cash), card,
                      loans)

    def _assert_rapper_unchanged(self, rapper, rapper_kwargs, keys):
        intact_rapper = self._new_rapper(**rapper_kwargs)
        for key in keys:
            self.assertEqual(getattr(rapper, key), getattr(intact_rapper, key))

    @staticmethod
    def _new_world(venues=None, store=None):
        if venues is None:
            venues = {'Charo': Venue('Charo', 1000, 2000, Melodic, 12)}
        if store is None:
            store = Store([Item('Gorra', 10, 10), Item('Reloj', 100, 100)])
        return World(venues, store)

    def test_buy_item_with_cash(self):
        cash, flow = 150, 300
        rapper_kwargs = dict(cash=cash, flow=flow)
        expected_cash, expected_flow = cash - 100, flow + 100
        rapper = self._new_rapper(**rapper_kwargs)
        world = self._new_world()
        ret, updates = world.act(rapper, 'buy_item', 'Reloj', CashPayment)
        # Assert no updates
        self.assertIsNone(updates)
        # Assert money cost and flow increase
        self.assertEqual(rapper.flow, expected_flow)
        self.assertEqual(rapper.wallet.cash, expected_cash)
        # Assert everything else remained the same
        attributes = ['credit_card', 'fans', 'loans', 'style']
        self._assert_rapper_unchanged(rapper, rapper_kwargs, attributes)

    def test_buy_item_with_cash_not_enough_cash(self):
        rapper = self._new_rapper(cash=50)
        world = self._new_world()
        with self.assertRaises(InsufficientMoneyException):
            world.act(rapper, 'buy_item', 'Reloj', CashPayment)

    def test_buy_item_with_credit_card(self):
        expenses, flow = 150, 300
        rapper_kwargs = dict(cc_current_expenses=expenses, flow=flow)
        expected_expenses, expected_flow = expenses + 100, flow + 100
        rapper = self._new_rapper(**rapper_kwargs)
        world = self._new_world()
        ret, updates = world.act(rapper, 'buy_item', 'Reloj', CreditPayment)
        # Assert no updates
        self.assertIsNone(updates)
        # Assert money cost and flow increase
        self.assertEqual(rapper.flow, expected_flow)
        self.assertEqual(rapper.credit_card.current_expenses,
                         expected_expenses)
        # Assert everything else remained the same
        attributes = ['fans', 'loans', 'style', 'wallet']
        self._assert_rapper_unchanged(rapper, rapper_kwargs, attributes)

    def test_buy_item_with_credit_card_not_enough_limit(self):
        rapper = self._new_rapper(cc_limit=50)
        world = self._new_world()
        with self.assertRaises(InsufficientLimitException):
            world.act(rapper, 'buy_item', 'Reloj', CreditPayment)

    def test_buy_invalid_item(self):
        rapper = self._new_rapper(cc_limit=50)
        world = self._new_world()
        with self.assertRaises(FlowException):
            world.act(rapper, 'buy_item', 'Inexistente', CashPayment)

    def test_concert(self):
        pass

    def test_concert_at_invalid_venue(self):
        pass

    def test_relax(self):
        pass

    def test_request_new_card_limit(self):
        new_limit = 2000
        rapper_kwargs = dict(cc_limit=1000)
        rapper = self._new_rapper(**rapper_kwargs)
        world = self._new_world()
        world.act(rapper, 'request_new_card_limit', new_limit)
        # Assert new limit
        self.assertEqual(rapper.credit_card.limit, new_limit)
        # Assert nothing else changed
        attributes = ['fans', 'loans', 'style', 'wallet']
        self._assert_rapper_unchanged(rapper, rapper_kwargs, attributes)


if __name__ == '__main__':
    main()
