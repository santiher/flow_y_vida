from unittest import TestCase, main
from unittest.mock import MagicMock

from flow.loan import Loan, LoanCancelled


class TestLoan(TestCase):

    def test_init(self):
        pass

    def test_as_dict(self):
        pass

    def test_request_loan_ok(self):
        pass

    def test_request_loan_rejected(self):
        pass

    def test_cancel_part_ok(self):
        pass

    def test_cancel_everything_ok(self):
        pass

    def test_cancel_insufficient_cash(self):
        pass

    def test_status(self):
        pass


if __name__ == '__main__':
    main()
