import random
from unittest import TestCase, main

from flow.constants import CashPayment
from flow.credit_card import CreditCard
from flow.exceptions import InsufficientMoneyException
from flow.rapper import Rapper
from flow.style import Lyric
from flow.wallet import Wallet


rapper_kwargs = dict(email='ray@flow.com', name='Ray Jones', fans=3200,
                     style=Lyric, flow=50, week=0, cash=50)


class TestRapper(TestCase):

    email = rapper_kwargs['email']
    name = rapper_kwargs['name']
    fans = rapper_kwargs['fans']
    style = rapper_kwargs['style']
    flow = rapper_kwargs['flow']
    cash = rapper_kwargs['cash']

    def test_init(self):
        rapper = Rapper(**self.rapper_kwargs)
        self.assertEqual(rapper.email, self.email)
        self.assertEqual(rapper.name, self.name)
        self.assertEqual(rapper.fans, self.fans)
        self.assertEqual(rapper.style, self.style)
        self.assertEqual(rapper.flow, self.flow)
        self.assertEqual(rapper.wallet.cash, self.cash)

    def test_as_dict(self):
        expected = {'email': self.email, 'name': self.name, 'fans': self.fans,
                    'style': self.style, 'flow': self.flow, 'week': 0,
                    'wallet': Wallet(self.cash).as_dict(),
                    'credit_card': CreditCard().as_dict(),
                    'loans': [], 'certificates_of_deposit': [],
                    'investments': [], 'image_url': 'http:home/ray.png'}
        rapper = Rapper(**self.rapper_kwargs, image_url='http:home/ray.png')
        self.assertEqual(rapper.as_dict(), expected)

    def test_acknowledge_new_fans(self):
        pass

    def test_buy(self):
        pass

    def test_charge(self):
        rapper = Rapper(**self.rapper_kwargs)
        with self.assertRaises(InsufficientMoneyException):
            rapper.charge(rapper.wallet.cash + 1, CashPayment)

    def test_pay(self):
        rapper = Rapper(**self.rapper_kwargs)
        money = random.randint(0, self.cash)
        rapper.pay(money)
        self.assertEqual(rapper.wallet.cash, self.cash + money)

    def test_perform_for(self):
        pass

    def test_relax(self):
        pass

    @property
    def rapper_kwargs(self):
        kwargs = dict(rapper_kwargs)
        kwargs['wallet'] = Wallet(kwargs.pop('cash'))
        return kwargs


if __name__ == '__main__':
    main()
