import random
from unittest import TestCase, main

from flow.exceptions import InsufficientMoneyException, FlowException
from flow.wallet import Wallet


class TestWallet(TestCase):

    args = cash, = 100,

    def test_init(self):
        wallet = Wallet(*self.args)
        self.assertEqual(wallet.cash, self.cash)

    def test_add_ok(self):
        wallet = Wallet(*self.args)
        money = random.randint(0, self.cash)
        self.assertEqual(wallet.add(money).cash, self.cash + money)

    def test_add_negative(self):
        wallet = Wallet(*self.args)
        with self.assertRaises(FlowException):
            wallet.add(-self.cash)

    def test_pay_ok(self):
        wallet = Wallet(*self.args)
        money = random.randint(0, self.cash - 10)
        self.assertEqual(wallet.pay(money).cash, self.cash - money)

    def test_pay_negative(self):
        wallet = Wallet(- self.cash)
        with self.assertRaises(FlowException):
            wallet.pay(1)

    def test_pay_not_enough_money(self):
        wallet = Wallet(*self.args)
        with self.assertRaises(InsufficientMoneyException):
            wallet.pay(self.cash + 1)

    def test_as_dict(self):
        expected = {'cash': self.cash}
        wallet = Wallet(*self.args)
        self.assertEqual(expected, wallet.as_dict())


if __name__ == '__main__':
    main()
