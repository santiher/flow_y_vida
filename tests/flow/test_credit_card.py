from unittest import TestCase, main

from flow.exceptions import FlowException, InsufficientLimitException


class TestStatement(TestCase):

    def test_init(self):
        pass

    def test_as_dict(self):
        pass


class TestCreditCard(TestCase):

    def test_init(self):
        pass

    def test_as_dict(self):
        pass

    def test_full_balance_current_period_balance(self):
        pass

    def test_full_balance_current_period_no_balance(self):
        pass

    def test_full_balance_new_period_balance(self):
        pass

    def test_full_balance_new_period_no_balance(self):
        pass

    def test_close_period_same_period_exception(self):
        pass

    def test_close_period_full_payment_no_interests(self):
        pass

    def test_close_period_full_payment_with_interests(self):
        pass

    def test_close_period_minimum_payment(self):
        pass

    def test_close_period_below_minimum_payment(self):
        pass

    def test_close_period_nothing_to_pay(self):
        pass

    def test_is_new_period_same_period(self):
        pass

    def test_is_new_period_new_period(self):
        pass

    def test_period_rate(self):
        pass

    def test_change_limit(self):
        pass

    def test_available_limit_ok(self):
        pass

    def test_available_limit_all_spent(self):
        pass

    def test_pay_not_enough_limit(self):
        pass

    def test_pay_ok(self):
        pass


if __name__ == '__main__':
    main()
