import glob
import os.path
import pathlib
from setuptools import setup


def files_in(path, recursive=False):
    return [os.path.join(*pathlib.Path(f).parts[1:])
            for f in glob.glob(path, recursive=False)]


def version():
    with open('flow/__init__.py', 'r') as f:
        for line in f:
            if line.startswith('__version__ ='):
                return line[15:-2]
    return '0.0.0'


setup(
    name='flow',
    version=version(),
    install_requires=['tornado', 'jinja2'],
    packages=['backend', 'flow', 'flow.server', 'storage'],
    scripts=glob.glob('scripts/*'),
    package_data={'backend': ['default.cfg'], 'flow.server': ['default.cfg'],
                  'flow': (files_in('flow/lang/*') +
                           files_in('flow/server/web/**', True))},
    entry_points={
        'console_scripts': [
            'flow_server = flow.server.server:main',
        ]},
)
