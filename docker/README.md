# Docker

## Running simple

Go to server and do `./docker.sh run`. It requires docker and docker-compose.

## Server

Contains docker files for running the server without using a database.

## Server postgres

Contains docker files for running the server and a PostgreSql database.  
It is not actually required at the moment.
