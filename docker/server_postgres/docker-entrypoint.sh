#!/bin/sh

case "${1}" in
  'example_server')
    shift
    example_server ${@}
  ;;
  'test')
    shift
    python setup.py test ${@}
  ;;
  'cli')
    shift
    bash ${@}
  ;;
  *)
    exec "${@}"
  ;;

esac
