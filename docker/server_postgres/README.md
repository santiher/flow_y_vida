# Running the bidder using docker

Install docker and docker-compose, set it up so that it works, you may have
to set it up so you can use it without sudo or some crap like that.

## Starting and stopping easy

`./docker.sh`

The server runs on `localhost:8888`

## Starting and stopping docker style

The first time starting it, it will have to download and build all the
necessary stuff, it may take a while.

Open the command line in the docker container after starting: `-e cli=true`

* Start: `docker-compose run --service-ports --rm api`
* Start and run tests: `docker-compose run --service-ports -e run=test --rm api`
* Start and run the server: `docker-compose --service-ports run -e run=server --rm api`
* Stop: `docker-compose stop`

## Accessing postgres

* Inside docker: `psql "host=postgresql dbname=test user=postgres password=password"`
* Outside docker: `psql "host=localhost dbname=test user=postgres password=password"`
