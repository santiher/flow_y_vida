#!/bin/bash

CMD=${1:-help}
CLI=${2:-0}

if [ "$CMD" == "help" ] ; then
    echo "    Help: ./run.sh [cmd] [cli]"
    echo ""
    echo "    cmds:"
    echo "    - start: starts everything but run nothing"
    echo "    - tests: run tests"
    echo "    - server: runs the server"
    echo "    - stop: stops everything"
    echo "    - rm: removes stopped containers"
    echo ""
    echo "    cli: true to open a command line inside the docker environment"
elif [ "$CMD" == "stop" ] ; then
    docker-compose stop;
elif [ "$CMD" == "rm" ] ; then
    docker-compose rm;
else
    docker-compose run --service-ports -e run=$CMD -e cli=$CLI --rm api;
fi
