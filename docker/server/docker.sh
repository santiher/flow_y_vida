#!/bin/bash

CMD=${1:-help}

if [ "$CMD" == "help" ] ; then
    echo "    Help: ./run.sh [cmd]"
    echo ""
    echo "    cmds:"
    echo "    - run: runs the server"
    echo "    - stop: stops everything"
    echo "    - rm: removes stopped containers"
    echo ""
elif [ "$CMD" == "stop" ] ; then
    docker-compose stop;
elif [ "$CMD" == "rm" ] ; then
    docker-compose rm;
elif [ "$CMD" == "run" ] ; then
    docker-compose run --service-ports --rm api;
fi
