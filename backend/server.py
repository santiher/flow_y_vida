import multiprocessing
import logging
import signal
import time

import tornado.httpserver
import tornado.ioloop
import tornado.netutil

from functools import partial
from tornado import version as tornado_version


logger = logging.getLogger('Server')


def run_server(make_app, app_settings, fork_method, single_process, port,
               max_wait, ssl_options):
    """ Run a tornado server
        @make_app: a function that receives app settings and returns a
                   tornado application.
                   Endpoints and handlers are defined here, check api.py.
        @app_settings: an dict like object with app settings.
        @fork_method: spawn, fork or forkserver for multi process servers
        @single_process: true for a single process server, false otherwise
        @port: port number the server listens at.
        @max_wait: max amount of seconds before the server closes after
                   receiving a close (to allow uncompleted requests to finish).
        @ssl_options: None for no ssl or a dictionary with certfile and keyfile
    """
    logger.info('Starting server')
    multiprocessing.set_start_method(fork_method)
    start_process = (main_single_process if single_process else
                     main_multi_process)
    start_process(make_app, app_settings, port, max_wait, ssl_options)


def main_single_process(make_app, app_settings, port, max_wait, ssl_options):
    app = make_app(app_settings)
    server = tornado.httpserver.HTTPServer(app)
    server.listen(port)
    servers = [server]
    if ssl_options:
        ssl_port = ssl_options.pop('port', 443)
        server = tornado.httpserver.HTTPServer(app, ssl_options=ssl_options)
        server.listen(ssl_port)
        servers.append(server)
    add_close_handler(servers, max_wait)
    tornado.ioloop.IOLoop.current().start()


def main_multi_process(make_app, app_settings, port, max_wait, ssl_options):
    app = make_app(app_settings)
    # Bind sockets
    sockets = tornado.netutil.bind_sockets(port)
    if ssl_options:
        ssl_port = ssl_options.pop('port', 443)
        ssl_sockets = tornado.netutil.bind_sockets(ssl_port)
    # Fork
    try:
        tornado.process.fork_processes(0)  # forks one process per cpu
    except KeyboardInterrupt:
        return  # Leave forker
    # Create server and add sockets
    server = tornado.httpserver.HTTPServer(app)
    server.add_sockets(sockets)
    servers = [server]
    if ssl_options:
        server = tornado.httpserver.HTTPServer(app, ssl_options=ssl_options)
        server.add_sockets(ssl_sockets)
        servers.append(server)
    add_close_handler(servers, max_wait)
    tornado.ioloop.IOLoop.current().start()


def add_close_handler(server, max_wait):
    for SIG in (signal.SIGINT, signal.SIGTERM):
        signal.signal(SIG, partial(close_handler, server, max_wait))


def close_handler(server, max_wait, sig, frame):
    logger.info('Preparing shutdown. Signal received: %s' % sig)
    close_function = partial(close, server, max_wait)
    tornado.ioloop.IOLoop.current().add_callback(close_function)


def close(server, max_wait):
    """ Wait until all requests are handled or until @max_wait passes.
        Then shutdown """
    logger.info('Shutting down: the server will shutdown in at most %s seconds'
                % max_wait)
    deadline = time.time() + max_wait
    server = server if isinstance(server, list) else [server]
    for s in server:
        s.stop()
    io_loop = tornado.ioloop.IOLoop.current()

    def stop_loop():
        now = time.time()
        tasks_running = (io_loop.handlers if tornado_version >= '5' else
                         (io_loop._callbacks or io_loop._timeouts))
        if now < deadline and tasks_running:
            io_loop.add_timeout(now + 1, stop_loop)  # Try again in 1 second
        else:
            io_loop.stop()
            logger.info('Shutdown')

    stop_loop()
