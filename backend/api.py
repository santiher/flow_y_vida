import tornado.web

import backend.handlers as handlers


def make_app(app_settings):
    return tornado.web.Application([
        (r'/dormir', handlers.AsyncHandler),
        (r'/auth', handlers.AuthHandler),
        (r'/user/(.*)', handlers.UserHandler),
        (r'/', handlers.MainHandler),
     ], **app_settings)
