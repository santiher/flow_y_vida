import asyncio
import logging
import logging.config
import os

from argparse import ArgumentParser
from ast import literal_eval
from concurrent.futures import ThreadPoolExecutor
from contextlib import contextmanager
from functools import wraps

from tornado.escape import json_decode
from tornado.log import access_log


class Bundle(dict):

    __slots__ = ()

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            return getattr(super(), name)

    def __setattr__(self, name, value):
        self[name] = value

    def __hasattr__(self, name):
        return name in self


class Config(Bundle):
    pass


def get_package_path():
    return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


@contextmanager
def as_file(file_or_path, *args, **kwargs):
    if type(file_or_path) is str:
        file = open(file_or_path, *args, **kwargs)
    else:
        file = file_or_path
    try:
        yield file
    finally:
        if type(file_or_path) is str:
            file.close()


def load_pyconfig(file_or_path, config=None):
    config = config or Config()
    with as_file(file_or_path) as cfg_file:
        exec(cfg_file.read(), config)
    return config


def load_config_and_logging(
        default_config=['%s/backend/default.cfg' % get_package_path()]):
    logging.basicConfig(level=logging.INFO)
    arg_parser = ArgumentParser()
    arg_parser.add_argument(
        '--config', '-c', action='append',
        help='Config files, executed in order inside the same scope.')
    args = arg_parser.parse_args()
    config = None
    for path in args.config or default_config:
        config = load_pyconfig(path, config)
    log_config = config.get('log_config')
    if log_config:
        with open(log_config, 'r') as f:
            log_config = literal_eval(f.read())
        logging.config.dictConfig(log_config)
    return config


def blocking_to_async(function, executor=None):
    ''' Use a threaded/process executor to handle blocking functions in it and
    return when they are done concurrently. Example:

    from concurrent.futures import ThreadPoolExecutor
    Executor = ThreadPoolExecutor(10)
    blocking_function = blocking_to_async(blocking_function, Executor)
    df = await blocking_function(query)
    '''
    event_loop = asyncio.get_event_loop()
    if executor is None:
        executor = ThreadPoolExecutor(10)

    @wraps(function)
    async def wrapper(*args, **kwargs):
        return await event_loop.run_in_executor(executor, function, *args,
                                                **kwargs)
    return wrapper


def json_decode_recursive(obj):
    def rec(obj):
        try:
            obj = json_decode(obj)
        except Exception:
            pass
        if isinstance(obj, list):
            return [rec(item) for item in obj]
        if isinstance(obj, dict):
            return {key: rec(obj[key]) for key in obj}
        return obj
    return rec(obj)


def log_request(handler):
    """Writes a completed HTTP request to the logs.
    Rewrite of tornado.web.Application.log_request to include the body

    Pass it to applications as *log_function*.
    """
    if handler.get_status() < 400:
        log_method = access_log.info
    elif handler.get_status() < 500:
        log_method = access_log.warning
    else:
        log_method = access_log.error
    request_time = 1000.0 * handler.request.request_time()
    body = handler.request.body.decode('utf8')
    if body:
        body = '  -  %s' % body
    log_method("%d %s %.2fms%s", handler.get_status(),
               handler._request_summary(), request_time, body)
