import sys
import traceback

from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta
from json import dumps, loads
from time import sleep

import jwt
from tornado.escape import json_encode, json_decode
from tornado.web import RequestHandler, HTTPError

from backend.auth import SECRET_KEY, jwtauth
from backend.utils import blocking_to_async


class MainHandler(RequestHandler):

    async def get(self):
        if 'Firefox' in self.request.headers.get('User-Agent'):
            self.write_csv([['nombre', 'edad'], ['pipo', 22], ['mica', 19]])
        else:
            self.write_json(dict(key='value'))

    def write_json(self, a_dictionary):
        self.set_header('Content-Type', 'application/json')
        self.content_type = 'application/json'
        self.write(json_encode(a_dictionary))

    def write_csv(self, a_list_of_lists):
        self.set_header('Content-Type', 'text/csv')
        self.set_header('content-Disposition',
                        'attachement; filename=file.csv')
        self.content_type = 'text/csv'
        self.write('\r\n'.join([','.join(['"%s"' % item for item in row])
                                for row in a_list_of_lists]).encode('utf8'))

    def write_error(self, status, **kwargs):
        etype, evalue, tb = sys.exc_info()
        exc_str = ''.join(traceback.format_exception(etype, evalue, tb))
        self.set_header('Content-Type', 'application/json')
        self.finish(dumps({**kwargs,
                           'error': {'code': status, 'message': exc_str}}))


class ErrorHandler(MainHandler):

    def prepare(self):
        raise HTTPError(status_code=404, reason="invalid path")


def verbose_sleep(seconds):
    sleep(seconds)
    return 'Slept %s seconds' % seconds


class AsyncHandler(MainHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        cls = self.__class__
        if not hasattr(cls, 'Executor'):
            cls.Executor = ThreadPoolExecutor(10)
            cls.async_sleep = blocking_to_async(verbose_sleep, cls.Executor)

    async def get(self):
        print('Going to sleep')
        ret = await AsyncHandler.async_sleep(5)
        print('Waking up message:', ret)
        self.write_json(dict(key='value'))


class AuthHandler(RequestHandler):
    """Handle to auth method.
       This method aims to provide a new authorization token
       There is a fake payload (for tutorial purpose)
    """

    from secrets import token_hex
    from hashlib import sha256

    users = {'pipo': {}}
    users['pipo']['salt'] = salt = token_hex(8)
    users['pipo']['pass'] = sha256((salt + '1234').encode('utf8')).hexdigest()

    @staticmethod
    def jtoken(user):
        """Encode a new token with JSON Web Token (PyJWT)"""
        encoded = jwt.encode(
            {'sub': user, 'exp': datetime.utcnow() + timedelta(seconds=600)},
            SECRET_KEY,  # Replace
            algorithm='HS256'
        ).decode('ascii')
        return encoded

    def post(self, *args, **kwargs):
        """Return the generated token"""
        data = json_decode(self.request.body)
        user, password = data.get('user'), data.get('password')
        salt = self.users.get(user, {}).get('salt', None)
        if salt:
            hpass = self.sha256((salt + password).encode('utf8')).hexdigest()
            expected = self.users[user].get('pass')
            if hpass == expected:
                response = {'token': self.jtoken(user)}
                self.write(response)


@jwtauth
class UserHandler(MainHandler):

    async def get(self, user, payload):
        payload = loads(payload)
        if payload.get('sub') == user:
            self.write_json(dict(result='ok'))
        else:
            self.write_json(dict(result='bad'))
